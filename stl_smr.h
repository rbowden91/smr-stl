/*
 * file:        stl_smr.h
 * description: SMR interface used by fake and real SMR
 */

#ifndef __STL_SMR_H__
#define __STL_SMR_H__

struct smr;
struct smr_op {
    lba_t lba;
    int band;
    int offset;
    int len;
    void *buf;
};

int smr_n_bands(struct smr *dev);
int smr_band_size(struct smr *dev);
int smr_write_pointer(struct smr *dev, int band);
int smr_init(char *dev, int band_size);
struct smr *smr_open(const char *name);
void smr_close(struct smr *dev);
void smr_read(struct smr *dev, unsigned band, unsigned offset, void *buf,
              unsigned n_sectors);
void smr_read_multi(struct smr *dev, struct smr_op *ops, int opcount);
void smr_write(struct smr *dev, unsigned band, unsigned offset,
               const void *buf, unsigned n_sectors);
void smr_write_multi(struct smr *dev, struct smr_op *ops, int opcount);
void smr_reset_pointer(struct smr *dev, unsigned band);
void smr_reset_all(struct smr *dev);
void smr_flush(struct smr *dev);

/* internal definitions for stl_*smr.c
 */
#ifdef _INTERNAL
struct smr {
    int fd;
    int n_bands;
    int band_size;
    int *write_pointers;
    pthread_mutex_t m;
    pthread_cond_t  c;
    int wp_sectors;             /* fakeSMR only */
};

#define SECTOR_SIZE 4096
#define BANDS_PER_SECTOR (SECTOR_SIZE/sizeof(int))
#endif

#endif
