CFLAGS=-g -Wall -O2
LIBS=-lpthread -laio 

.PHONY: libsmr-fake.a libsmr.a

all: stl format mkfakesmr stl-plugin.so format-fake stl-fake-plugin.so

stl: stl_map.o stl_base.o rb.o stl_test.o stl_realsmr.o stl_smr.o sgio.o
	gcc -g $^ -o $@ $(LIBS)

stl-fake: stl_map.o stl_base.o rb.o stl_test.o stl_fakesmr.o stl_smr.o sgio.o
	gcc -g $^ -o $@ $(LIBS)

format: format.o stl_realsmr.o sgio.o stl_smr.o
	gcc -g $^ -o $@ $(LIBS)

format-fake: format.o stl_fakesmr.o stl_smr.o sgio.o
	gcc -g $^ -o $@ $(LIBS)

mkfakesmr: mkfakesmr.o stl_fakesmr.o stl_smr.o
	gcc -g $^ -o $@ $(LIBS)

libsmr-fake.a: stl_map.o stl_base.o rb.o stl_fakesmr.o stl_smr.o
	rm -f $@
	ar rvs $@ $^

libsmr.a: stl_map.o stl_base.o rb.o stl_realsmr.o sgio.o stl_smr.o
	rm -f $@
	ar rvs $@ $^

#realsmr: sgio.o stl_realsmr.o 
#	gcc -g $^ -o $@

clean:
	rm -f *.o stl stl2

SHARED_OBJS = stl-plugin.shared.o stl_map.shared.o stl_base.shared.o \
	rb.shared.o stl_realsmr.shared.o sgio.shared.o
stl-plugin.so: $(SHARED_OBJS)
	gcc -shared -fPIC -DPIC $^ -o $@ $(LIBS)

SHARED_FAKE_OBJS = stl-plugin.shared.o stl_map.shared.o stl_base.shared.o \
	rb.shared.o stl_fakesmr.shared.o sgio.shared.o
stl-fake-plugin.so: $(SHARED_FAKE_OBJS)
	gcc -shared -fPIC -DPIC $^ -o $@ $(LIBS)

%.shared.o : %.c
	gcc -fPIC -DPIC -g -c $^ -o $@ -DRBTEST 

rb.o: rb.c
	gcc -g -c rb.c -DRBTEST 


