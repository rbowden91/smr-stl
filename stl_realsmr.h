/*
 * file:        stl_realsmr.h
 * description: real SMR functions
 */
#include <sys/types.h>

#ifndef __STL_REALSMR_H__
#define __STL_REALSMR_H__

#define lba28_limit ((__u64)(1<<28) - 1)

struct smr;
struct iovec;

int do_reset_write_pointer (int fd, __u64 lba, int all_bit);
int do_report_zones(int fd, __u64 lba, void *data,
                    unsigned int data_bytes);

int smr_n_bands(struct smr *dev);
int smr_band_size(struct smr *dev);
int smr_write_pointer(struct smr *dev, int band);
struct smr *smr_open(const char *name);
void smr_close(struct smr *dev);
void smr_read(struct smr *dev, unsigned band, unsigned offset, void *buf,
              unsigned n_sectors);
void smr_readv(struct smr *dev, unsigned band, unsigned offset,
               const struct iovec *iov, int iovcnt);
void smr_write(struct smr *dev, unsigned band, unsigned offset, const void *buf,
               unsigned n_sectors);
void smr_writev(struct smr *dev, unsigned band, unsigned offset,
                const struct iovec *iov, int iovcnt);
void smr_reset_pointer(struct smr *dev, unsigned band);
void smr_reset_all(struct smr *dev);

#endif
