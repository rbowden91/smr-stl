#!/usr/bin/python

import sys
addr = -10
count = 0
val = 0

for line in sys.stdin:
    x,a,n,v = line.split()
    a,n,v = map(int, [a,n,v])
    if a != addr+count or v != val:
        if addr >= 0:
            print "verify", addr, count, val
        addr,count,val = a,n,v
    else:
        count += n
