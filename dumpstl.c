/*
 * file:        dumpstl.c
 * description: print out contents of disk image.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <getopt.h>

#include "stl.h"
#include "stl_base.h"
#include "stl_smr.h"

char buf[10*SECTOR_SIZE];

int n_bands;
int band_size;
int group_size;
int group_span;
int n_groups;
int map_size;

int addr(int band, int offset)
{
    return (band*band_size + offset)*SECTOR_SIZE;
}

char *record_type(unsigned t)
{
    char *names[] = {"*BAD*", "BAND", "MAP", "DATA", "NULL"};
    if (t > RECORD_NULL)
        return "*BAD*";
    return names[t];
}
    
int num_sectors(int type, int count)
{
    if (type == RECORD_MAP) {
        int map_per_sector = SECTOR_SIZE / sizeof(struct map_record);
        return (count + map_per_sector - 1) / map_per_sector;
    }
    if (type == RECORD_BAND) {
        int band_per_sector = SECTOR_SIZE / sizeof(struct band_record);
        return (count + band_per_sector - 1) / band_per_sector;
    }
    printf("ERROR: external records in non-MAP non-BAND entry\n");
    return 0;
}

struct parse {
    int offset;
    int len;
    int fmt;
    char *name;
};

#define PARSE_ENTRY(strct, field, fmt, name)                                \
    {offsetof(struct strct, field), sizeof(((struct strct*)0)->field), fmt, name}

void print_structure(char *banner, void *ptr, struct parse *table)
{
    printf("%s\n", banner);
    while (table->name != NULL) {
        if (table->fmt == 'P') {
            pba_t *pba = ptr + table->offset;
            printf("%s : %d.%d\n", table->name, pba->band, pba->offset);
        }
        else {
            int val = 0;
            if (table->len == 2)
                val = *(short*)(ptr + table->offset);
            else
                val = *(int*)(ptr + table->offset);
            printf("%s : %d\n", table->name, val);
        }
        table++;
    }
    printf("\n");
}

void print_bands(struct smr *dev, struct superblock *sb, void *buf)
{
    int i, w;
    printf("\nnon-empty bands:\n");
    for (i = sb->map_size+1; i < sb->n_bands; i++) 
        if ((w = smr_write_pointer(dev, i)) > 0) {
            smr_read(dev, i, w-1, buf, 1);
            struct header *h = buf;
            printf("%d offset %d seq %d\n", i, w-1, h->seq);
        }
}

void print_ckpts(struct smr *dev, struct superblock *sb, int band,
                 int offset, void *buf)
{
    do {
        smr_read(dev, band, offset, buf, 1);
        struct header *h = buf;
        printf("%d %d base %d.%d seq %d records %d\n",
	       band, offset, h->base.band,
               h->base.offset, h->seq, h->records);
        band = h->next.band;
        offset = h->next.offset;
    } while (offset < smr_write_pointer(dev, band));
}

void chase_frontiers(struct smr *dev, struct superblock *sb, int band,
                     int offset, void *buf)
{
    struct header *h = buf;
    int i;
    printf("chase %d %d\n", band, offset);
    while (offset < smr_write_pointer(dev, band)) {
	smr_read(dev, band, offset, buf, 1);
	printf("%d.%d: %d records", band, offset, h->local_records);
	struct map_record *m = (void*)(h+1);
	for (i = 0; i < h->local_records; i++) 
	    printf(" %lld->%d.%d", m[i].lba, m[i].pba.band, m[i].pba.offset);
	printf("\n");
	band = h->next.band;
	offset = h->next.offset;
    }
}

void print_frontiers(struct smr *dev, struct superblock *sb, int band,
                     int offset, void *buf)
{
    int i, j = 0;
    do {
        smr_read(dev, band, offset, buf, 1);
        struct header *h = buf;
        if (h->next.band == band && h->next.offset != offset+1 && h->type == RECORD_BAND) {
            printf("%d.%d (%d):\n", band, offset, h->seq);
            int len = h->next.offset - offset - 1;
            void *buf2 = valloc(len * 4096);
            smr_read(dev, band, offset+1, buf2, len);
            struct band_record *b = buf2;
            for (i = 0; i < h->records; i++)
                if (b[i].type == BAND_TYPE_FRONTIER)
                    printf("%d %d.%d: %d\n", j, b[i].band, b[i].write_pointer, h->seq);
            free(buf2);
            j++;
        }
        band = h->next.band;
        offset = h->next.offset;
    } while (offset < smr_write_pointer(dev, band));
}

int main(int argc, char **argv)
{
    void *buf = valloc(4096);
    
    struct smr *dev = smr_open(argv[1]);
    if (dev == NULL)
        perror("open"), exit(1);
    argc -= 2;
    argv += 2;

    smr_read(dev, 0, 0, buf, 1);
    struct superblock sb = *(struct superblock*)buf;

    if (sb.magic != STL_MAGIC)
        printf("superblock: bad magic\n"), exit(1);

    
    struct parse sb_parse[] = {
        PARSE_ENTRY(superblock, disk_size,  'i', "disk size (x4K))"),
        PARSE_ENTRY(superblock, n_bands,    'i', "num bands  "),
        PARSE_ENTRY(superblock, band_size,  'i', "band size (x4K)  "),
        PARSE_ENTRY(superblock, map_size,   'i', "map size (bands)"),
        PARSE_ENTRY(superblock, group_size, 'i', "group size (bands)"),
        PARSE_ENTRY(superblock, group_span, 'i', "group span (LBA)"),
        {0,0,0,0}
    };
    print_structure("superblock", &sb, sb_parse);

    struct parse hdr_parse[] = {
        PARSE_ENTRY(header, seq,  'i', "sequence #"),
        PARSE_ENTRY(header, local_records,  'i', "local records"),
        PARSE_ENTRY(header, records,  'i', "external records"),
        PARSE_ENTRY(header, next,  'P', "next"),
        PARSE_ENTRY(header, prev,  'P', "prev"),
        PARSE_ENTRY(header, base,  'P', "base"),
        {0, 0, 0, 0}
    };

    int i, w;
    for (i = 1; i <= sb.map_size; i++) {
        w = smr_write_pointer(dev, i);
        if (w > 0) {
            printf("write_pointer(%d) : %d\n", i, w);
            smr_read(dev, i, 0, buf, 1);
            printf("%d ", i);
            print_structure("map band", buf, hdr_parse);
        }
    }

    while (argc > 0) {
        if (!strcmp(argv[0], "bands")) {
            print_bands(dev, &sb, buf);
            argv++, argc--;
        }
        else if (!strcmp(argv[0], "ckpts")) {
            int band = atoi(argv[1]),
                offset = atoi(argv[2]);
            argv += 3, argc -= 3;
            print_ckpts(dev, &sb, band, offset, buf);
        }
        else if (!strcmp(argv[0], "frontiers")) {
            int band = atoi(argv[1]),
                offset = atoi(argv[2]);
            argv += 3, argc -= 3;
            print_frontiers(dev, &sb, band, offset, buf);
        }
        else if (!strcmp(argv[0], "chase")) {
            int band = atoi(argv[1]),
                offset = atoi(argv[2]);
            argv += 3, argc -= 3;
            chase_frontiers(dev, &sb, band, offset, buf);
        }
    }
    
    return 0;
}
