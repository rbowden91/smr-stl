/*
 * file:        stl_base.c
 * description: basic shingle translation layer logic
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/uio.h>
#include <pthread.h>
#include <assert.h>
#include <stdarg.h>
#include <malloc.h>
#include <sys/time.h>

/* Apple OSX rbtree implementation
 */
#include "rbtree.h"

#include "stl.h"
#include "stl_map.h"
#include "stl_smr.h"
#include "stl_base.h"
#include "stl_public.h"

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#ifdef DEBUG
#define DEBUG_PRINT(...) fprintf( stderr, __VA_ARGS__ )
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#endif

/* There are two levels of cleaning and band allocation.
 * - background cleaning - clean until MINFREE_BG bands are free,
 *   allocate extents at normal priority. (clean if 1 free)
 * - forced cleaning - clean until MINFREE_PRIO bands are free,
 *   allocate at high priority. (i.e. can allocate the last band that
 *   is not touched by normal priority allocation)
 */
#define MINFREE_FG 2
#define MINFREE_BG 4
#define PRIO_NORM 0
#define PRIO_HIGH 1

double get_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec/1.0e6;
}

static FILE *log_fp;

int stl_loginit(const char *logfile)
{
    log_fp = fopen(logfile, "w");
    return log_fp != NULL;
}

void stl_log(const char *fmt, ...)
{
    if (log_fp == NULL)
        return;
    va_list ap;
    va_start(ap, fmt);
    vfprintf(log_fp, fmt, ap);
    va_end(ap);
}

/* todo - sequence number arithmetic with signed sequence #s?
 * or just use 64-bit sequence number?
 */

/*---------- Prototypes -------------*/

static void do_write(struct volume *v, int group, lba_t lba,
                     const void *buf, int sectors, int prio);

static int clean_group(struct volume *v, int g, int minfree, int prio);

static void _stl_write(struct volume *v, lba_t lba, const void *buf, int sectors);

/*----------- Helper functions for band/offset PBAs ---------------*/

int64_t pba2long(struct volume *v, pba_t pba)
{
    return (int64_t)pba.band * v->band_size + pba.offset;
}

pba_t long2pba(struct volume *v,  int64_t addr)
{
    return (struct pba){.band = addr / v->band_size,
            .offset = addr % v->band_size};
}


#define PBA_NEXT (struct pba){.band = 0xFFFFFFFF, .offset=0xFFFFFFFF}


/* Update mapping. Removes any total overlaps, edits any partial
 * overlaps, adds new extent to forward and reverse map.
 */
static void update_range(struct volume *v, pba_t location, lba_t lba, int len,
                         pba_t pba)
{
    assert(pba.offset != 0);
    assert(len > 0);

    struct entry *e = stl_map_lba_geq(v->map, lba);
    if (e != NULL) {
        /* [----------------------]        e     new     new2
         *        [++++++]           -> [-----][+++++][--------]
         */
        if (e->lba < lba && e->lba+e->len > lba+len) {
            int64_t new_lba = lba+len;
            int new_len = e->lba + e->len - new_lba;
            assert(new_len > 0);

            pba_t new_pba = e->pba;
            if (!pba_eq(e->pba, PBA_INVALID))
                new_pba = pba_add(e->pba, (e->len - new_len));
            struct entry *_new2 = stl_map_entry(sizeof(struct entry));
            *_new2 = (struct entry){.lba = lba+len, .pba = new_pba,
                                    .len = new_len, .dirty = 1};

            DEBUG_PRINT("split %d,+%d -> %d.%d into ",
                  (int)e->lba, e->len, e->pba.band, e->pba.offset);

            e->len = lba - e->lba; /* do this *before* inserting below */
            DEBUG_PRINT("%d,+%d -> %d.%d %d,+%d -> %d.%d\n",
                  (int)e->lba, e->len, e->pba.band, e->pba.offset,
                 (int)_new2->lba, _new2->len, _new2->pba.band, _new2->pba.offset);
            stl_map_update(e, e->lba, e->pba, e->len);
            e->dirty = 1;
            assert(e->len > 0);
            stl_map_insert(v->map, _new2, _new2->lba, _new2->pba, _new2->len);
            e = _new2;
        }
        /* [------------]
         *        [+++++++++]        -> [------][+++++++++]
         */
        else if (e->lba < lba) {
            DEBUG_PRINT("overlap tail %d,+%d -> %d.%d becomes", (int)e->lba, e->len,
                  e->pba.band, e->pba.offset);
            e->len = lba - e->lba;
            DEBUG_PRINT(" %d,+%d -> %d.%d\n", (int)e->lba, e->len,
                  e->pba.band, e->pba.offset);
            stl_map_update(e, e->lba, e->pba, e->len);
            assert(e->len > 0);
            e->dirty = 1;
            e = stl_map_lba_iterate(v->map, e);
        }
        /*          [------]
         *   [+++++++++++++++]        -> [+++++++++++++++]
         */
        while (e != NULL && e->lba+e->len <= lba+len) {
            DEBUG_PRINT("overwriting %d,+%d -> %d.%d\n",  (int)e->lba, e->len,
                  e->pba.band, e->pba.offset);
            struct entry *tmp = stl_map_lba_iterate(v->map, e);
            stl_map_remove(v->map, e);
            e = tmp;
        }
        /*          [------]
         *   [+++++++++]        -> [++++++++++][---]
         */
        if (e != NULL && lba+len > e->lba) {
            DEBUG_PRINT("overlap head %d,+%d -> %d.%d becomes", (int)e->lba, e->len,
                  e->pba.band, e->pba.offset);
            int n = (lba+len)-e->lba;
            e->lba += n;
            if (!pba_eq(e->pba, PBA_INVALID))
                e->pba.offset += n;
            e->len -= n;
            DEBUG_PRINT(" %d,+%d -> %d.%d\n", (int)e->lba, e->len,
                  e->pba.band, e->pba.offset);
            stl_map_update(e, e->lba, e->pba, e->len);
            assert(e->len > 0);
            e->dirty = 1;
        }
    }

    /* When TRIM is passed down from host_trim, location=0,0 - set it
     * in the map, where it gets logged by checkpoint and then
     * removed from the map. When we read it on startup, location!=0,0
     * - all the work was done above by clearing the LBA range.
     */
    if (!pba_eq(pba, PBA_INVALID) || pba_eq(location, PBA_NULL)) {
        struct entry *_new = stl_map_entry(sizeof(*_new));
        *_new = (struct entry){.lba = lba, .pba = pba, .len = len,
                               .dirty = 1};
        stl_map_insert(v->map, _new, lba, pba, len);
    }
}

/*------------------- Initialization ---------------------*/

/* note that this works perfectly for TRIM logging, as well.
 */
static void read_map_record(struct volume *v, pba_t location,
                            struct map_record *m, uint32_t seq)
{
    assert(m->pba.band < v->n_bands &&
           (m->pba.offset+m->len) <= v->band_size &&
           (m->lba+m->len) <= v->group_span*v->n_groups);
    /* tag range with location it's checkpointed in */
    update_range(v, location, m->lba, m->len, m->pba);
}

static int group_of(struct volume *v, int band)
{
    return (band - 1 - v->map_size) / v->group_size;
}

static void read_band_record(struct volume *v, struct band_record *b,
                             uint32_t seq)
{
    assert(b->band < v->n_bands && b->type < BAND_TYPE_MAX);
    int i = b->band;
    v->band[i].type = b->type;
    v->band[i].dirty = 0;
    if (b->type == BAND_TYPE_FRONTIER) {
        v->groups[group_of(v, i)].frontier = i;
        v->groups[group_of(v, i)].frontier_offset = max(0, b->write_pointer - 1);
    }
}

static pba_t read_records(struct volume *v, pba_t location)
{
    void *buffer = NULL;
    buffer = valloc(SECTOR_SIZE);
    memset(buffer, 0, SECTOR_SIZE);
    struct header *h = buffer;
    smr_read(v->disk, location.band, location.offset, buffer, 1);
    assert(h->magic == STL_MAGIC);

    int i;
    uint32_t seq = h->seq;
    struct header h0 = *h;      /* saved copy */

    int nsectors = h0.next.offset - location.offset - 1;
    void *buf = NULL;

    if (h0.records > 0) {
        buf = valloc(nsectors * SECTOR_SIZE);
        memset(buf, 0, nsectors * SECTOR_SIZE);
    }
    if (h0.type == RECORD_BAND) {
        struct band_record *r = (void*)(h+1);
        for (i = 0; i < h->local_records; i++)
            read_band_record(v, &r[i], seq);
        if (nsectors != 0){
            smr_read(v->disk, location.band, location.offset+1, buf, nsectors);
        }
        int hmax = nsectors * SECTOR_SIZE / sizeof(struct band_record);
        r = buf;
        for (i = 0; i < h0.records && i < hmax; i++)
            read_band_record(v, &r[i], seq);
    }
    if (h0.type == RECORD_MAP) {
        struct map_record *m = (void*)(h+1);
        for (i = 0; i < h->local_records; i++)
            read_map_record(v, location, &m[i], seq);
        if(nsectors>0){
            smr_read(v->disk, location.band, location.offset+1, buf, nsectors);
        }
        int hmax = nsectors * SECTOR_SIZE / sizeof(struct map_record);
        m = buf;
        for (i = 0; i < h0.records && i < hmax; i++)
            read_map_record(v, location, &m[i], seq);
    }

    if (buf)
        free(buf);
    if (buffer)
        free(buffer);
    v->map_prev = location;
    return h0.next;
}


#if 0
static int valid_pba(struct volume *v, pba_t pba)
{
    if (pba.band < 0 || pba.offset < 0)
        return 0;
    return pba.band < v->n_bands && pba.offset < v->band_size;
}
#endif

/* locking: not needed, since init_volume hasn't returned yet
 */
static int chase_frontiers(struct volume *v)
{
    void *buffer = NULL;
    buffer = valloc(SECTOR_SIZE);
    memset(buffer, 0, SECTOR_SIZE);
    struct header *h = buffer;
    int i, max_seq = 0;

    for (i = 0; i < v->n_groups; i++) {
        int f = v->groups[i].frontier;
        if (v->band[f].write_pointer == 0) {
            continue;
        }
        int offset = v->groups[i].frontier_offset;
        smr_read(v->disk, f, offset, buffer, 1);

        /* we really should try to handle bad data here, but it's hard...
         */
        while (h->next.offset < v->band[h->next.band].write_pointer){
            v->groups[i].prev = mkpba(f, offset);
            if (f != h->next.band) {
                assert(v->band[f].type == BAND_TYPE_FRONTIER);
                v->groups[i].count[BAND_TYPE_FULL]++;
		assert(v->groups[i].count[BAND_TYPE_FULL] <= v->group_size);

                v->groups[i].count[BAND_TYPE_FREE]--;
		assert(v->groups[i].count[BAND_TYPE_FREE] >= 0);

                v->band[f].type = BAND_TYPE_FULL;
                f = h->next.band;
                v->band[f].type = BAND_TYPE_FRONTIER;
                v->groups[i].frontier = f;
            }
            smr_read(v->disk, h->next.band, h->next.offset, buffer, 1);

            if (h->local_records > 0) {
                 read_map_record(v, PBA_NULL, (struct map_record*) (h+1), h->seq);
            }
            if (h->seq > max_seq) {
                max_seq = h->seq;
            }
        }
        if (f != h->next.band) {
            assert(v->band[f].type == BAND_TYPE_FRONTIER);
            v->groups[i].count[BAND_TYPE_FULL]++;
	    assert(v->groups[i].count[BAND_TYPE_FULL] <= v->group_size);

            v->groups[i].count[BAND_TYPE_FREE]--;
	    assert(v->groups[i].count[BAND_TYPE_FREE] >= 0);

            v->band[f].type = BAND_TYPE_FULL;
            f = h->next.band;
            v->band[f].type = BAND_TYPE_FRONTIER;
            v->groups[i].frontier = f;
        }
        //v->groups[i].frontier_offset = h->next.offset;
        v->groups[i].frontier_offset = v->band[f].write_pointer;
    }
    v->seq = max_seq;
    if (buffer)
        free(buffer);
    return 1;
}

void *defrag_thread(void *ctx)
{
    struct volume *v = ctx;
    struct timeval tv;
    int i, g = 0;

    while (1) {
	usleep(100000);		/* 400ms */
	gettimeofday(&tv, NULL);
	int idle = (tv.tv_sec - v->last_op.tv_sec +
		    (tv.tv_usec - v->last_op.tv_usec) * 1.0e-6 > 0.5);
	pthread_mutex_lock(&v->m);
	off_t extents = stl_map_count(v->map);
	pthread_mutex_unlock(&v->m);
	off_t vsize = v->n_groups * v->group_span; /* volume size in sectors */

	/* target is 1 extent per 6MB or less, 6MB = 1536 sectors*/
	int n = (extents * 1536ULL) / vsize;

        if (!idle)
            n = n/2;
        for (i = 0; i < n; i++) {
            defrag_group(v, g++);
            if (g >= v->n_groups)
                g = 0;
        }
    }
}

void *cleaning_thread(void *ctx)
{
    int i;
    struct volume *v = ctx;
    struct timeval tv;

    while (1) {
	usleep(100000);		/* 100ms */
	gettimeofday(&tv, NULL);
	if (tv.tv_sec - v->last_op.tv_sec +
	    (tv.tv_usec - v->last_op.tv_usec) * 1.0e-6 > 1.0) {
	    int min_i = -1, min_free = v->group_size+1;

	    pthread_mutex_lock(&v->m);
	    for (i = 0; i < v->n_groups; i++) {
		int n = v->groups[i].count[BAND_TYPE_FREE];
		if (n < min_free) {
		    min_free = n;
		    min_i = i;
		}
	    }

	    assert(min_i >= 0);
	    clean_group(v, min_i, MINFREE_BG, PRIO_NORM);
	    pthread_mutex_unlock(&v->m);
	}
    }
    return 0;
}

struct volume *stl_open(const char *dev)
{
    int i, j, k, seq, m = 0;
    struct volume *v = calloc(sizeof(*v), 1);
    void *buffer = NULL;
    buffer = valloc(SECTOR_SIZE);
    memset(buffer, 0, SECTOR_SIZE);

    pthread_mutexattr_t attr;   /* god I hate Posix threads... */
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&v->m, &attr);
    pthread_cond_init(&v->c, NULL);
    v->defrag_begin = v->defrag_end = -1;
    v->map = stl_map_init();

    if ((v->disk = smr_open(dev)) == NULL)
        return NULL;

    smr_read(v->disk, 0, 0, buffer, 1); /* read 1 sector */
    struct superblock *sb = buffer;
    assert(sb->magic == STL_MAGIC);

    /* note that we assume all bands are equal-sized. (ignore last one?)
     */
    v->band_size = smr_band_size(v->disk);
    v->n_bands = smr_n_bands(v->disk);
    v->band = calloc(v->n_bands * sizeof(v->band[0]), 1);
    for (i = 0; i < v->n_bands; i++)
        v->band[i].write_pointer = smr_write_pointer(v->disk, i);

    v->map_size = sb->map_size;
    v->group_size = sb->group_size;
    v->group_span = sb->group_span;
    v->n_groups = sb->n_groups;
    v->groups = calloc(v->n_groups * sizeof(*v->groups), 1);

    /* Find the current map band - i.e. the one starting with the
     * highest sequence number.
     */
    struct header *h = buffer;
    for (i = 1, seq = -1; i <= v->map_size; i++) {
        if (v->band[i].write_pointer == 0)
            continue;
        smr_read(v->disk, i, 0, buffer, 1);
        if (h->magic == STL_MAGIC && (int)h->seq > seq) {
            seq = h->seq;
            m = i;
        }
    }
    assert(seq > -1 && m > 0);

    /* Find the last record in the current map band by searching backwards from
     * write_pointer-1.  Offset of last legal header will be in 'i'.
     */
    v->map_band = m;
    for (i = v->band[m].write_pointer - 1; i > 0; i--) {

        smr_read(v->disk, m, i, buffer, 1);

        if (h->magic == STL_MAGIC && h->seq >= seq && h->next.band == m)
            break;
    }
    assert(i != -1);
    // ->band[v->map_band].write_pointer = i;
    v->seq = h->seq+1;

    /* now read forward from earliest relevant record to the end of
     * the checkpointed map and band information. Set the volume base
     * accordingly.
     */
    smr_read(v->disk, m, i, buffer, 1);
    pba_t base = v->base = h->base;
    seq = h->seq;
    while (base.band != m || base.offset != i)
        base = read_records(v, base);

    /* now we have the band map and the exception map as of the last
     * checkpoint. For now assume that drive is properly checkpointed.
     * In the future, log recovery proceeds in breadth-first / seq#
     * order starting with the tail of all the bands marked 'current'.
     */

    /* recover the state of all of the band groups.
     */
    for (i = 0, j = 1+v->map_size; i < v->n_groups; i++) {
        for (k = 0; k < v->group_size; j++, k++) {
            int type = v->band[j].type;
            if (type == BAND_TYPE_FRONTIER)
                v->groups[i].frontier = j;
            v->groups[i].count[type]++;
	    assert(v->groups[i].count[type] <= v->group_size);
        }
    }

    if (chase_frontiers(v))
        checkpoint_volume(v, CKPT_FULL);
    if (buffer)
        free(buffer);

    pthread_create(&v->th1, NULL, cleaning_thread, v);
    pthread_create(&v->th2, NULL, defrag_thread, v);
    return v;
}


/* Get size of unopened volume in 4K sectors - analogous to stat(2).st_size
 * note that this will not replay the log or write to the volume in any way.
 */
off_t stl_size(const char *name)
{
    lba_t val = -1;
    struct smr *disk;
    if ((disk = smr_open(name)) == NULL)
        return -1;

    struct superblock *sb = valloc(SECTOR_SIZE);
    smr_read(disk, 0, 0, sb, 1); /* read 1 sector */

    if (sb->magic == STL_MAGIC)
        val = sb->group_span * sb->n_groups;

    free(sb);
    smr_close(disk);

    return val;
}


/* Get size of opened volume in 4K sectors - analogous to fstat(2).st_size
 */
off_t stl_vsize(struct volume *v)
{
    return v->n_groups * v->group_span;
}


void stl_close(struct volume *v)
{
    stl_map_destroy(v->map);
    smr_close(v->disk);
    pthread_cancel(v->th1);
    pthread_cancel(v->th2);
    free(v->band);
    free(v->groups);
}

/* ---------- Cleaning ----------- */

int cmp_ops_pba(const void *arg1, const void *arg2)
{
    const struct smr_op *op1 = arg1;
    const struct smr_op *op2 = arg2;
    if (op1->band != op2->band)
        return op1->band - op2->band;
    return op1->offset - op2->offset;
}

int cmp_ops_lba(const void *arg1, const void *arg2)
{
    const struct smr_op *op1 = arg1;
    const struct smr_op *op2 = arg2;
    return op1->lba - op2->lba;
}

/* find the densest 32MB LBA region and re-write it. If typical track
 * is 1.6MB, then one extent per 8MB gives a read slowdown of <=17%.
 */
void defrag_group(struct volume *v, int g)
{
    extern double get_time(void);
    lba_t begin = g * v->group_span;
    int chunksz = 8*1024*1024 / SECTOR_SIZE; /* magic number warning */
    int n_regions = (v->group_span + chunksz - 1) / chunksz;
    int extents[n_regions], mass[n_regions];

    memset(extents, 0, sizeof(extents));
    memset(mass, 0, sizeof(mass));

    pthread_mutex_lock(&v->m);
    double t1 = get_time();
    struct entry *e = stl_map_lba_geq(v->map, begin);
    while (e != NULL && e->lba < begin + v->group_span) {
        int i = (e->lba - begin) / chunksz;
        extents[i]++;
        mass[i] += e->len;      /* not quite correct */
        int i2 = (e->lba + e->len - begin) / chunksz;
        if (i2 != i)
            extents[i2]++;
        e = stl_map_lba_iterate(v->map, e);
    }

    int max = 0, k = 0, i;
    for (i = 0; i < n_regions; i++) {
        if (extents[i] > max) {
            max = extents[i];
            k = i;
        }
    }

    if (max < 4 || mass[k] < chunksz/4) { /* magic number alert */
        pthread_mutex_unlock(&v->m);
        return;
    }

    printf("coalesce group %d : %d extents %d sectors %d\n",
           g, max, mass[k], stl_map_count(v->map));

    /* don't go off the end of the group.
     */
    lba_t end = begin + (k+1)*chunksz;
    if (end > begin + v->group_span)
        end = begin + v->group_span;
    begin = begin + k*chunksz;

    void *buf = valloc(chunksz*SECTOR_SIZE);
    memset(buf, 0, chunksz*SECTOR_SIZE);
    struct smr_op ops[max+100];

    for (i = 0, e = stl_map_lba_geq(v->map, begin);
         e != NULL && e->lba < end;
         i++, e = stl_map_lba_iterate(v->map, e)) {

        /* skip negative entries for TRIM.
         */
        if (pba_eq(PBA_INVALID, e->pba))
            continue;
        
        /* consider the case:
         *  +----- extent --------+
         *                    ^------- 8MB chunk -----^
         *  |<..... skip ....>|
         */
        lba_t _lba = max(begin, e->lba);
        lba_t skip = _lba - e->lba;
        ops[i].lba = _lba;
        ops[i].buf = buf + (_lba - begin)*SECTOR_SIZE;
        ops[i].band = e->pba.band;
        ops[i].offset = e->pba.offset + skip;
        ops[i].len = e->len - skip;
        if (e->lba + e->len > end)
            ops[i].len = end - e->lba;
    }

    /* only block reads/writes that interfere with this range
     */
    v->defrag_begin = begin;
    v->defrag_end = end;
    pthread_mutex_unlock(&v->m);
    
    qsort(ops, i, sizeof(ops[0]), cmp_ops_pba);

    double t2 = get_time();
    smr_read_multi(v->disk, ops, i);
    double t3 = get_time();
    _stl_write(v, begin, buf, end - begin);

    pthread_mutex_lock(&v->m);
    v->defrag_begin = v->defrag_end = -1;
    pthread_cond_broadcast(&v->c);
    pthread_mutex_unlock(&v->m);
    
    double t4 = get_time();
    printf("coal time: %.4f %.4f %.4f\n", t2-t1, t3-t2, t4-t3);
    free(buf);

    checkpoint_volume(v, CKPT_INCR);
}

static void do_write_multi(struct volume *v, int group,
                           struct smr_op *ops, int n_ops, int prio);

/* clean a single group. Returns true if cleaning was performed.
 */
static int clean_group(struct volume *v, int g, int minfree, int prio)
{
    int i, nfree, made_changes = 0, iters = 0;
    int base = g * v->group_size + v->map_size + 1;
    extern double get_time(void);

    double t1 = get_time();
    //printf("clean group %d %d %d\n", g, minfree, prio);
    stl_log("clean_group %d %d %d\n", g, minfree, prio);

    /* are there enough free bands?
     */
    for (i = nfree = 0; i < v->group_size; i++)
        if (v->band[base+i].type == BAND_TYPE_FREE)
            nfree++;

    assert(nfree == v->groups[g].count[BAND_TYPE_FREE]);

    while (v->groups[g].count[BAND_TYPE_FREE] <= minfree) {
        DEBUG_PRINT("CLEANING %d: free = %d iter %d\n", g,
               v->groups[g].count[BAND_TYPE_FREE], ++iters);
        made_changes = 1;

        assert(iters < 10);

        /* find the cleaning cost of each band, by counting:
         * - total data (sectors) in band
         */
#define SECTORS_PER_SEEK 128
        int *seeks = calloc(v->group_size * sizeof(int), 1);
        int *sectors = calloc(v->group_size * sizeof(int), 1);

        pba_t begin = mkpba(base, 0);
        struct entry *e = stl_map_pba_geq(v->map, begin);

        while (e != NULL && e->pba.band < base+v->group_size) {
            i = e->pba.band - base;
            assert(i >= 0 && i < v->group_size);
            seeks[i]++;
            sectors[i] += e->len;
            e = stl_map_pba_iterate(v->map, e);
        }

        /* mark all the free bands so we don't try to clean them
         */
        for (i = 0; i < v->group_size; i++)
            if (v->band[i+base].type == BAND_TYPE_FREE ||
                v->band[i+base].type == BAND_TYPE_FRONTIER)
                sectors[i] = 1e9;

        int min_cost = 1e9, min_band = -1;
        for (i = 0; i < v->group_size; i++) {
#if 0
            int s = min(seeks[i], 1 + v->band_size / 512);
            if (iters > 1)
                s = 0;
            int cost = s*SECTORS_PER_SEEK + sectors[i];
#endif
            int cost = sectors[i];
            if (cost < min_cost) {
                min_cost = cost;
                min_band = i;
            }
        }
        assert(min_band >= 0);

        /* read all the valid extents from this band
         */
        int band = min_band + base, n_sectors = sectors[min_band];
        int n_extents = seeks[min_band];
        void *buf = valloc(n_sectors * SECTOR_SIZE);
        memset(buf, 0, n_sectors * SECTOR_SIZE);
        int *len = calloc(n_extents * sizeof(int), 1);
        int64_t *lba = calloc(n_extents * sizeof(int64_t), 1);
        void *ptr = buf;

        DEBUG_PRINT("PICKED %d - %d sectors\n", band, n_sectors);

	if (n_extents > 0) {
	    struct smr_op *ops = malloc(n_extents * sizeof(*ops));
	    begin = mkpba(band, 0);
	    e = stl_map_pba_geq(v->map, begin);
	    for (i = 0; e != NULL && e->pba.band == band; i++) {
		DEBUG_PRINT("moving %d.%d -> %d+%d\n", e->pba.band,
			    e->pba.offset, (int)e->lba, e->len);
		ops[i] = (struct smr_op){
		    .lba = e->lba, .band = e->pba.band,
		    .offset = e->pba.offset, .len = e->len, .buf = ptr};
		ptr += (e->len * SECTOR_SIZE);
		struct entry *tmp = stl_map_pba_iterate(v->map, e);
		stl_map_remove(v->map, e);
		e = tmp;
	    }

	    assert(i == n_extents);
	    smr_read_multi(v->disk, ops, n_extents);

	    qsort(ops, i, sizeof(ops[0]), cmp_ops_lba);

	    /* Now re-write them
	     */
	    do_write_multi(v, g, ops, i, prio);
	    free(ops);
	}

        int type = v->band[band].type;

        v->groups[g].count[type]--;
	assert(v->groups[i].count[type] >= 0);
        v->groups[g].count[BAND_TYPE_FREE]++;
	assert(v->groups[i].count[BAND_TYPE_FREE] <= v->group_size);

        v->band[band].type = BAND_TYPE_FREE;
        v->band[band].dirty = 1;
        v->band[band].write_pointer = 0;
        smr_reset_pointer(v->disk, band);

        free(buf);
        free(seeks); free(sectors);
        free(len); free(lba);
        nfree++;
	checkpoint_volume(v, CKPT_FULL);
    }
    //printf(" clean: %.4f\n", get_time()-t1);
    return made_changes;
}

void clean_all(struct volume *v)
{
    int g, dirty;
    for (g = dirty = 0; g < v->n_groups; g++)
        dirty = dirty || clean_group(v, g, MINFREE_BG, PRIO_NORM);
    if (dirty)
        checkpoint_volume(v, CKPT_FULL);
}

/*------------ Write, allocate -----------*/

/* Find a free band in group 'g'.
 * Only grab the last band if 'prio' is true.
 */
static int find_free_band(struct volume *v, int g, int prio)
{
    int threshold = prio ? 0 : MINFREE_FG;
    int i, j = 1 + v->map_size + g*v->group_size, n;
    for (i = n = 0; i < v->group_size; i++, j++) {
        if (v->band[j].type == BAND_TYPE_FREE) {
            if (n >= threshold)
                return j;
            n++;
        }
    }
    return -1;
}

/* assemble the header for a write packet into buffer
 */
static void fill_write_hdr(int seq, pba_t here, pba_t prev, pba_t next,
                           pba_t base, void *map, int n_records, void *buffer)
{
    memset(buffer, 0, SECTOR_SIZE);
    struct header *h = buffer;
    DEBUG_PRINT("fill_write_hdr: %d.%d (%d.%d)\n", here.band, here.offset,
          next.band, next.offset);
    *h = (struct header){.magic = STL_MAGIC, seq,
                         .type = RECORD_DATA, .local_records = n_records,
                         .records = 0, .prev = prev, .next = next,
                         .base = base};
    memcpy(h+1, map, sizeof(struct map_record)*n_records);
}

/* allocate a PBA extent. Updates the band map by advancing the write
 * pointer by 'len' sectors, marking it dirty, and setting sequence
 * number. Priority is passed down to find_free_band to avoid deadlock
 * on forced cleaning.
 *
 * locking: v->m is held when calling this.
 */
static pba_t alloc_extent(struct volume *v, int g, int len,
                    int prio, int *plen)
{
    struct group *gr = v->groups + g;
    int left, b;
top:
    b = gr->frontier;
    left = v->band_size - v->band[b].write_pointer;
    pba_t here = {.band = b, .offset = v->band[b].write_pointer};
    if (left < 8) {
        int b2 = find_free_band(v, g, prio);
        if (b2 == -1) {
            if (clean_group(v, g, MINFREE_FG, PRIO_HIGH))
                checkpoint_volume(v, CKPT_FULL);
            goto top;
        }
        assert(b2 != -1);
        gr->count[BAND_TYPE_FREE]--;
	assert(gr->count[BAND_TYPE_FREE] >= 0);

        assert(v->band[b2].write_pointer == 0);
        pba_t next = {.band = b2, .offset = 0};

        gr->count[BAND_TYPE_FULL]++;
	assert(gr->count[BAND_TYPE_FULL] <= v->group_size);

        v->band[b].type = BAND_TYPE_FULL;
        v->band[b].dirty = 1;

        void *buffer = valloc(SECTOR_SIZE);
        fill_write_hdr(v->seq++, here, v->groups[g].prev, next, v->base,
                       NULL /* no map entry */, 0, buffer);
        v->groups[g].prev = here;
        smr_write(v->disk, here.band, here.offset, buffer, 1);
        free(buffer);

        v->band[b].write_pointer++;

        v->band[b2].type = BAND_TYPE_FRONTIER;
        v->band[b2].dirty = 1;

        gr->frontier = b2;
        b = b2;
        here = next;
        left = v->band_size;
        //checkpoint_volume(v);
    }

    /* caller is responsible for updating write_pointer
     */
    len = min(left-2, len);
    *plen = len;
    DEBUG_PRINT("alloc: returning %d.%d %d\n", b, v->band[b].write_pointer, len);
    return here;
}

/* actually perform a write, wrapped with DATA records. 'prio' is used
 * to ensure that writes for forced cleaning can grab the last free
 * band.
 *
 */
static void do_write(struct volume *v, int group, lba_t lba,
                     const void *buf, int sectors, int prio)
{
    void *buf1 = valloc(SECTOR_SIZE*2), *buf2 = buf1+SECTOR_SIZE;

    int alloced = 0;
    while (sectors > 0) {
        pthread_mutex_lock(&v->m); /* vvvvvvvvvvvvvvvvvvvvvv */
        pba_t pba = alloc_extent(v, group, sectors+2, prio, &alloced);
        int _sectors = alloced-2;
        struct map_record map = {.lba = lba, .pba = pba_add(pba,1),
                                 .len = _sectors};

        /* map entries haven't been logged yet, so location=PBA_NULL
         */

        fill_write_hdr(v->seq++, pba, v->groups[group].prev,
                       pba_add(pba, _sectors+1), v->base,
                       NULL /* no map entry */, 0, buf1);

        v->groups[group].prev = pba;

        fill_write_hdr(v->seq++, pba_add(pba, 1+_sectors),
                       v->groups[group].prev, pba_add(pba, _sectors+2),
                       v->base, &map, 1, buf2);
        v->groups[group].prev = pba_add(pba, 1+_sectors);

        v->band[pba.band].write_pointer += alloced;
        v->band[pba.band].dirty = 1;

        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^^ */

        int b = pba.band, o = pba.offset;
        struct smr_op ops[] = {
            {.band = b, .offset = o, .len = 1, .buf = buf1},
            {.band = b, .offset = o+1, .len = _sectors, .buf = (void*)buf},
            {.band = b, .offset = o+1+_sectors, .len = 1, .buf = buf2}
        };

        smr_write_multi(v->disk, ops, 3);

        pthread_mutex_lock(&v->m); /* vvvvvvvvvvvvvvvvvvvvvv */
        update_range(v, PBA_NULL, lba, _sectors, pba_add(pba, 1));
        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^^ */

        sectors -= _sectors;
        lba += _sectors;
        buf += _sectors * SECTOR_SIZE;
    }
    free(buf1);
}

static int sum_ops(struct smr_op *ops, int n_ops)
{
    int i, sum;
    for (i = sum = 0; i < n_ops; i++)
        sum += ops[i].len;
    return sum;
}

/* this is only used for cleaning, so we don't need to include the map
 * in the trailer - we're going to checkpoint this immediately anyway.
 */
static void do_write_multi(struct volume *v, int group,
                           struct smr_op *ops, int n_ops, int prio)
{
    void *buf1 = valloc(SECTOR_SIZE*2), *buf2 = buf1+SECTOR_SIZE;
    struct smr_op *_ops = malloc(sizeof(*_ops)*n_ops);
    int alloced = 0, sectors = sum_ops(ops, n_ops);

    int i = 0; /* ops[i] */
    while (sectors > 0) {
        pthread_mutex_lock(&v->m); /* vvvvvvvvvvvvvvvvvvvvvv */
        pba_t pba = alloc_extent(v, group, sectors+2, prio, &alloced);
        int _sectors = alloced-2, mapped = 0;
        int borrowed = 0;

        struct smr_op *p = _ops;
        *p++ = (struct smr_op){.band = pba.band, .
                               offset = pba.offset, .len = 1, .buf = buf1};

        pba_t here = pba_add(pba, 1);
        while (i < n_ops && mapped < _sectors) {
            *p++ = (struct smr_op){.band = here.band, .offset = here.offset,
                                   .len = ops[i].len - borrowed,
                                   .buf = ops[i].buf + (borrowed*SECTOR_SIZE)};
            borrowed = 0;
            here.offset += ops[i].len;
            mapped += ops[i].len;
            i++;
        }

        borrowed = mapped - _sectors;
        if (borrowed > 0) {
            p[-1].len -= borrowed;
            i--;                /* revisit this op in the next write */
        }
        *p++ = (struct smr_op){.band = here.band, .offset = here.offset,
                               .len = 1, .buf = buf2};
        int n = p - _ops;

        /* map entries haven't been logged yet, so location=PBA_NULL
         */

        fill_write_hdr(v->seq++, pba, v->groups[group].prev,
                       pba_add(pba, _sectors+1), v->base,
                       NULL /* no map entry */, 0, buf1);

        v->groups[group].prev = pba;

        fill_write_hdr(v->seq++, pba_add(pba, 1+_sectors),
                       v->groups[group].prev, pba_add(pba, _sectors+2),
                       v->base, NULL /* no map entry */, 0, buf2);
        v->groups[group].prev = pba_add(pba, 1+_sectors);

        v->band[pba.band].write_pointer += alloced;
        v->band[pba.band].dirty = 1;

        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^^ */

        smr_write_multi(v->disk, _ops, n);

        pthread_mutex_lock(&v->m); /* vvvvvvvvvvvvvvvvvvvvvv */
        int k;
        for (k = 1; k < n-1; k++)
            update_range(v, PBA_NULL, _ops[k].lba, _ops[k].len,
                         mkpba(_ops[k].band, _ops[k].offset));
        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^^ */
    }
    free(buf1);
    free(_ops);
}

static void _stl_write(struct volume *v, lba_t lba, const void *buf, int sectors)
{
    /* internal writes can't span a group boundary
     */
    while (sectors > 0) {
        int group = lba / v->group_span;
        int _sectors = min(sectors, (group+1) * v->group_span - lba);
        do_write(v, group, lba, buf, _sectors, PRIO_NORM);
        lba += _sectors;
        sectors -= _sectors;
        buf += (_sectors*SECTOR_SIZE);
    }
}

void stl_write(struct volume *v, lba_t lba, const void *buf, int sectors)
{
    assert(lba + sectors <= v->n_groups * v->group_span);

    /* http://stackoverflow.com/questions/9225567/how-to-print-a-int64-t-type-in-c */
    stl_log("host_write %" PRId64 " %d\n", lba, sectors);
    DEBUG_PRINT("host_write %" PRId64 " %d\n", lba, sectors);

    gettimeofday(&v->last_op, NULL);

    void *newbuf = NULL;
    if (((long)buf & 511) != 0) {
        newbuf = memalign(512, sectors * SECTOR_SIZE);
        memcpy(newbuf, buf, sectors * SECTOR_SIZE);
        buf = (const void*)newbuf;
    }

    /* block if there's an interfering defrag operation
     */
    pthread_mutex_lock(&v->m);
    while (max(v->defrag_begin, lba) < min(v->defrag_end, lba+sectors))
        pthread_cond_wait(&v->c, &v->m);
    pthread_mutex_unlock(&v->m);
    
    _stl_write(v, lba, buf, sectors);

    /* checkpoint every 2000 writes
    */
    pthread_mutex_lock(&v->m);
    if (++(v->count) % 2000 == 0) {
        if (v->count >= 20000) {
            checkpoint_volume(v, CKPT_FULL);
            v->count = 0;
        }
        else {
            checkpoint_volume(v, CKPT_INCR);
        }
    }
    pthread_mutex_unlock(&v->m);

    if (newbuf)
        free(newbuf);

    gettimeofday(&v->last_op, NULL);
}


void stl_trim(struct volume *v, lba_t lba, int sectors)
{
    assert(lba + sectors <= v->n_groups * v->group_span);

    stl_log("host_trim %" PRId64 " %d\n", lba, sectors);
    DEBUG_PRINT("host_trim %" PRId64 " %d\n", lba, sectors);

    /* internal ops can't span a group boundary
     */
    while (sectors > 0) {
        pba_t null_pba = {.band = -1, .offset = -1};
        int group = lba / v->group_span;
        int _sectors = min(sectors, (group+1) * v->group_span - lba);

        /* map entries haven't been logged yet, so location=PBA_NULL
         */
        pthread_mutex_lock(&v->m); /* vvvvvvvvvvvvvvvvvvvvvv */
        update_range(v, PBA_NULL, lba, _sectors, null_pba);
        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^^ */
        lba += _sectors;
        sectors -= _sectors;
    }
}

void stl_flush(struct volume *v)
{
    smr_flush(v->disk);
}

/*----------- Read logic --------------*/

void stl_read(struct volume *v, lba_t lba, void *buf, int sectors)
{
    assert(lba + sectors <= v->n_groups * v->group_span);

    stl_log("host_read %" PRId64 " %d\n", lba, sectors);
    DEBUG_PRINT("host_read %" PRId64 " %d\n", lba, sectors);

    void *oldbuf = buf, *buf2 = NULL;
    if (((long)buf & 511) != 0)
        buf = buf2 = memalign(512, sectors * SECTOR_SIZE);

    gettimeofday(&v->last_op, NULL);

    while (sectors > 0) {
        pthread_mutex_lock(&v->m);   /* vvvvvvvvvvvvvvvvvvvv */
        struct entry *e = stl_map_lba_geq(v->map, lba);
        pthread_mutex_unlock(&v->m); /* ^^^^^^^^^^^^^^^^^^^^ */
        if (e == NULL || e->lba >= lba + sectors) {
            memset(buf, 0, sectors * SECTOR_SIZE);
            return;
        }
        int offset = max(lba - e->lba, -sectors), len = -offset;
        if (offset < 0) {
            memset(buf, 0, len*SECTOR_SIZE);
            sectors -= len;
            lba += len;
            buf += len * SECTOR_SIZE;
            offset = 0;
        }
        len = min(sectors, e->len - offset);
        if (len > 0) {
            if (pba_eq(e->pba, PBA_INVALID))
                memset(buf, 0, len * SECTOR_SIZE);
            else
                smr_read(v->disk, e->pba.band, e->pba.offset+offset, buf, len);
        }
        sectors -= len;
        lba += len;
        buf += len * SECTOR_SIZE;
    }
    if (buf2 != NULL) {
        memcpy(oldbuf, buf2, buf-buf2);
        free(buf2);
    }

    gettimeofday(&v->last_op, NULL);
}

/*----------- Map checkpointing --------------*/

static pba_t fill_meta(int type, int n_records, pba_t location, pba_t prev,
		       pba_t next, pba_t base, int seq, void *buffer)
{
    memset(buffer, 0, SECTOR_SIZE);
    struct header *h = buffer;
    if (pba_eq(next, PBA_NEXT))
	next = mkpba(location.band, location.offset+1);
    DEBUG_PRINT("fill_meta: %d %d base %d.%d\n", type, n_records,
                base.band, base.offset);
    *h = (struct header){.magic = STL_MAGIC, .seq = seq,
                         .type = type, .local_records = 0,
                         .records = n_records, .prev = prev,
                         .next = next, .base = base};
    return location;
}

static const int map_per_sector = SECTOR_SIZE / sizeof(struct map_record);
static const int band_per_sector = SECTOR_SIZE / sizeof(struct band_record);


void check_for_space(struct volume *v, int n_sectors)
{
    /* make sure there's enough room in the current map band.
     */
    int i = v->map_band;
    int next_band = (i >= v->map_size) ? 1 : i+1;
    int needed = n_sectors + 2;
    if (v->band[i].write_pointer + needed >= v->band_size) {
	void *buffer = valloc(SECTOR_SIZE);

	v->map_prev = fill_meta(RECORD_NULL, 0,
				mkpba(i, v->band[i].write_pointer),
				v->map_prev, mkpba(next_band, 0),
				v->base, v->seq++, buffer);
	smr_write(v->disk, i, v->band[i].write_pointer, buffer, 1);

        v->map_band = next_band;
        v->band[next_band].write_pointer = 0;
        smr_reset_pointer(v->disk, next_band);
	free(buffer);
    }
}

/* write map updates and then band updates into map band.
 */

/* this works even for non-power-of-2 sizes */
#define ROUND_UP(n, size) (((n+size-1)/size)*size)

// first do full checkpoint
void checkpoint_volume(struct volume *v, enum ckpt type)
{
    void *buffer = valloc(SECTOR_SIZE);
    int i, n_records, m = v->map_band;
    pba_t new_base = PBA_INVALID;

    stl_log("checkpoint_volume %d\n", v->count);
    DEBUG_PRINT("checkpoint_volume %d\n", v->count);

    /* write out all the band records
     */
    int band_len = ROUND_UP(v->n_bands * sizeof(struct band_record),
			    SECTOR_SIZE);
    struct band_record *bands = valloc(band_len);
    memset(bands, 0, band_len);

    for (i = 1+v->map_size, n_records = 0; i < v->n_bands; i++)
        if (type == CKPT_FULL || v->band[i].dirty) {
	    bands[n_records++] = (struct band_record)
                {.band = i, .type = v->band[i].type,
		 .write_pointer = v->band[i].write_pointer};
            v->band[i].dirty = 0;
        }


    if (n_records > 0) {
        int n_sectors = (n_records * sizeof(struct band_record) +
			 SECTOR_SIZE - 1) / SECTOR_SIZE;
        check_for_space(v, n_sectors);

	m = v->map_band;
        new_base = mkpba(m, v->band[m].write_pointer);
        pba_t next = mkpba(m, (v->band[m].write_pointer + 1 + n_sectors));

	v->map_prev = fill_meta(RECORD_BAND, n_records,
				mkpba(m, v->band[m].write_pointer),
				v->map_prev, next, v->base, v->seq++, buffer);
	smr_write(v->disk, m, v->band[m].write_pointer++, buffer, 1);

        smr_write(v->disk, m, v->band[m].write_pointer, bands, n_sectors);
        v->band[m].write_pointer += n_sectors;

	v->map_prev = fill_meta(RECORD_BAND, 0,
				mkpba(m, v->band[m].write_pointer),
				v->map_prev, PBA_NEXT, v->base, v->seq++,
				buffer);
	smr_write(v->disk, m, v->band[m].write_pointer++, buffer, 1);
    }

    free(bands);

    /* checkpoint map entries. always checkpoint the dirty ones.
     */
    int n_map = stl_map_count(v->map);
    int map_len = ROUND_UP(n_map * sizeof(struct map_record), SECTOR_SIZE);
    struct map_record *map = valloc(map_len);
    memset(map, 0, map_len);

    m = v->map_band;

    struct entry *e = stl_map_lba_iterate(v->map, NULL);
    n_records = 0;
    while (e != NULL) {
        struct entry *tmp = stl_map_lba_iterate(v->map, e);
        if (type == CKPT_FULL || e->dirty) {
            map[n_records++] = (struct map_record)
                {.lba = e->lba, .pba = e->pba, .len = e->len};
	    e->dirty = 0;
            if (pba_eq(e->pba, PBA_INVALID)) {/* TRIM gets logged once */
                stl_map_remove(v->map, e);   /* and then removed */
            }
	}
        e = tmp;
    }

    /* don't write anything if nothing changed.
     */
    if (n_records > 0) {
        DEBUG_PRINT("checkpoint at %d\n", v->seq);
        for (i = 0; i < n_records; i++)
            DEBUG_PRINT(" %d,+%d -> %d.%d\n", (int)map[i].lba, map[i].len,
                  map[i].pba.band, map[i].pba.offset);
        int n_sectors = (n_records * sizeof(struct map_record) +
			 SECTOR_SIZE - 1) / SECTOR_SIZE;

        check_for_space(v, n_sectors);

	int m = v->map_band;
        pba_t next =  mkpba(m, (v->band[m].write_pointer + n_sectors + 1));

	v->map_prev = fill_meta(RECORD_MAP, n_records,
				mkpba(m, v->band[m].write_pointer),
				v->map_prev, next, v->base, v->seq++, buffer);
	smr_write(v->disk, m, v->band[m].write_pointer++, buffer, 1);

        smr_write(v->disk, m, v->band[m].write_pointer, map, n_sectors);
        v->band[m].write_pointer += n_sectors;

        if (type == CKPT_FULL) {
            DEBUG_PRINT("base %d.%d -> %d.%d\n", v->base.band, v->base.offset,
                        new_base.band, new_base.offset);
            v->base = new_base;
        }

	v->map_prev = fill_meta(RECORD_MAP, 0,
				mkpba(m, v->band[m].write_pointer),
				v->map_prev, PBA_NEXT, v->base, v->seq++,
				buffer);
	smr_write(v->disk, m, v->band[m].write_pointer++, buffer, 1);
    }
    free(map);

    free(buffer);
}

/*------------ the rest --------------*/


