/*
 * file:        stl_public.h
 * description: public definitions for stl functions
 *
 * requires: <stdint.h> - int64_t
 */

#ifndef __STL_PUBLIC_H__
#define __STL_PUBLIC_H__

struct volume;
typedef int64_t lba_t;
#define STL_SECTOR_SIZE 4096

struct volume *stl_open(const char *dev);
lba_t          stl_size(const char *dev);
void           stl_close(struct volume *v);
void           stl_write(struct volume *v, lba_t lba, const void *buf, int sectors);
void           stl_trim(struct volume *v, lba_t lba, int sectors);
void           stl_read(struct volume *v, lba_t lba, void *buf, int sectors);
lba_t          stl_vsize(struct volume *v);
void           stl_flush(struct volume *v);

int            stl_loginit(const char *logfile);
void           stl_log(const char *fmt, ...);

#endif
