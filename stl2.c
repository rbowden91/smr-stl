
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
//#include <sys/rbtree.h>
#include "rbtree.h"

#include "stl2.h"

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

/*----------- Data Structures ------------*/
/* a band group is a self-sufficient STL with an LBA span and a set of
 * bands.
 */
struct group {
    int count[BAND_TYPE_MAX];   /* todo - keep up to date */
    int frontier;
};

/* track band info so that we can log write pointer changes back to
 * the map. Not sure how important this is on real SMR...
 */
struct band {
    uint16_t type;              /* BAND_TYPE_FREE, etc. */
    uint16_t dirty;
    uint32_t write_pointer;     /* next free sector */
    uint32_t seq;               /* of last write it's persisted in */
};

/* the primary data structure. Forward and reverse maps, geometry,
 * band info, group into, etc.
 */
struct volume {
    rb_tree_t map;
    rb_tree_t rmap;
    int   fd;
    int   band_size;
    int   n_bands;
    struct band *band;
    int   map_size;             /* number of rotating map bands */
    int   map_band;             /* current persistent map band */
    int   map_offset;           /* and offset */
    int   group_size;
    int   group_span;
    int   n_groups;
    struct group *groups;
    void *buf;                  /* temporary buffer */
    int   seq;
    struct pba base;            /* TODO - set this */
    int   oldest_seq;
    struct pba map_prev;
};

/* todo - sequence number arithmetic with signed sequence #s?
 * or just use 64-bit sequence number?
 */

/* 'location' is the header of the checkpoint packet that this entry
 * was last written in. Not valid if the entry is dirty.
 */

struct entry {
    rb_node_t  rb_fwd;          /* LBA -> PBA map */
    rb_node_t  rb_rev;          /* PBA -> LBA map */
    int64_t    lba;
    struct pba pba;
    struct pba location;        /* TODO - set this properly */
    int32_t    len;
    uint32_t   seq;
    uint8_t    dirty;
};

/*---------- Prototypes -------------*/

void checkpoint_volume(struct volume *v);
static void do_write(struct volume *v, int group, int64_t lba,
                     void *buf, int sectors, int prio);

/*----------- Low-level disk operations -----------*/

void sector_op(struct volume *v, int wr, int band, int offset,
               void *buf, int nsectors)
{
    off_t byte_offset = SECTOR_SIZE * (v->band_size * band + offset);
    off_t byte_len = nsectors * SECTOR_SIZE;
    int val;
    assert((byte_offset + byte_len - 1) / (v->band_size * SECTOR_SIZE) == band);
    if (wr)
        val = pwrite(v->fd, buf, byte_len, byte_offset);
    else
        val = pread(v->fd, buf, byte_len, byte_offset);
    if (val < 0)
        perror("op"), assert(0);
}
void write_sectors(struct volume *v, struct pba pba, void *buf, int len)
{
    sector_op(v, 1, pba.band, pba.offset, buf, len);
}
void read_sectors(struct volume *v, struct pba pba, void *buf, int len)
{
    sector_op(v, 0, pba.band, pba.offset, buf, len);
}

/*----------- Helper functions for band/offset PBAs ---------------*/

int64_t pba2long(struct volume *v, struct pba pba)
{
    return (int64_t)pba.band * v->band_size + pba.offset;
}

struct pba long2pba(struct volume *v,  int64_t addr)
{
    return (struct pba){.band = addr / v->band_size,
            .offset = addr % v->band_size};
}

struct pba pba_add(struct volume *v, struct pba pba, int offset)
{
    return long2pba(v, pba2long(v, pba) + offset);
}

struct pba pba(int band, int offset)
{
    return (struct pba){.band = band, .offset = offset};
}

#define PBA_EQUAL(a, b) (a.band == b.band && a.offset == b.offset)

#define PBA_NEXT (struct pba){.band = 0xFFFFFFFF, .offset=0xFFFFFFFF}


/* ----------- LBA MAP FUNCTIONS ------------- */

/* returns equal if the LBA intervals overlap
 */
signed int compare_nodes_fwd(void *ctx, const void *n1, const void *n2)
{
    const struct entry *e1 = n1, *e2 = n2;
    if (e1->lba + e1->len <= e2->lba)
        return -1;
    if (e1->lba >= e2->lba + e2->len)
        return 1;
    return 0;
}

/* returns equal if the key (lba) is contained in the interval
 */
signed int compare_key_fwd(void *ctx, const void *n1, const void *key)
{
    const struct entry *e = n1;
    const int32_t *p = key;

    if (e->lba + e->len <= *p)
        return -1;
    if (e->lba > *p)
        return 1;
    return 0;
}

rb_tree_ops_t fwd_ops = {
    .rbto_compare_nodes = compare_nodes_fwd,
    .rbto_compare_key = compare_key_fwd,
    .rbto_node_offset = 0,
    .rbto_context = 0
};

/* returns equal if the PBA intervals overlap
 */
signed int compare_nodes_rev(void *ctx, const void *n1, const void *n2)
{
    struct volume *v = ctx;
    const struct entry *e1 = n1, *e2 = n2;
#if 0
    if (e1->pba.band != e2->pba.band)
        return e1->pba.band - e2->pba.band;
    if (e1->pba.offset + e1->len <= e2->pba.offset)
        return -1;
    if (e1->pba.offset >= e2->pba.offset + e2->len)
        return 1;
#endif
    
    int64_t pba1 = pba2long(v, e1->pba);
    int64_t pba2 = pba2long(v, e2->pba);
    if (pba1 + e1->len <= pba2)
        return -1;
    if (pba1 >= pba2 + e2->len)
        return 1;

    assert(0);                  /* should never happen */
    return 0;
}

/* returns equal if the key (PBA) is contained in the interval
 */
signed int compare_key_rev(void *ctx, const void *n1, const void *key)
{
    struct volume *v = ctx;
    const struct entry *e = n1;
    const struct pba *p = key;
    int64_t pba_e = pba2long(v, e->pba);
    int64_t pba_k = pba2long(v, *p);
    if (pba_e + e->len <= pba_k)
        return -1;
    if (pba_e > pba_k)
        return 1;
    return 0;
}

rb_tree_ops_t rev_ops = {
    .rbto_compare_nodes = compare_nodes_rev,
    .rbto_compare_key = compare_key_rev,
    .rbto_node_offset = sizeof(rb_node_t),
    .rbto_context = 0
};

/* Update mapping. Removes any total overlaps, edits any partial
 * overlaps, adds new extent to forward and reverse map.
 */
void update_range(struct volume *v, int64_t lba, int len,
                  struct pba pba, uint32_t seq)
{
    assert(pba.offset != 0);
    assert(len > 0);
    
    struct entry *e = rb_tree_find_node_geq(&v->map, &lba);
    if (e != NULL) {
        /* [----------------------]        e     new     new2
         *        [++++++]           -> [-----][+++++][--------]
         */
        if (e->lba < lba && e->lba+e->len > lba+len) {
            int64_t new_lba = lba+len;
            int new_len = e->lba + e->len - new_lba;
            struct pba new_pba = pba_add(v, e->pba, (e->len - new_len));
            struct entry *_new2 = malloc(sizeof(*_new2));

            assert(new_len > 0);
            *_new2 = (struct entry){.lba = lba+len, .pba = new_pba,
                                    .len = new_len, .seq = seq, .dirty = 1};

            e->len = lba - e->lba; /* do this *before* inserting below */
            e->dirty = 1;
            e->seq = seq;
            assert(e->len > 0);
            rb_tree_insert_node(&v->map, _new2);
            rb_tree_insert_node(&v->rmap, _new2);
            e = _new2;
        }
        /* [------------]
         *        [+++++++++]        -> [------][+++++++++]
         */
        else if (e->lba < lba) {
            e->len = lba - e->lba;
            assert(e->len > 0);
            e->dirty = 1;
            e->seq = seq;
            e = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
        }
        /*          [------]
         *   [+++++++++++++++]        -> [+++++++++++++++]
         */
        while (e != NULL && e->lba+e->len <= lba+len) {
            struct entry *tmp = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
            rb_tree_remove_node(&v->map, e);
            rb_tree_remove_node(&v->rmap, e);
            free(e);
            e = tmp;
        }
        /*          [------]
         *   [+++++++++]        -> [++++++++++][---]
         */
        if (e != NULL && lba+len > e->lba) {
            int n = (lba+len)-e->lba;
            e->lba += n;
            e->pba.offset += n;
            e->len -= n;
            assert(e->len > 0);
            e->seq = seq;
            e->dirty = 1;
        }
    }
    /* pass an invalid PBA (-1,-1) to indicate TRIM.
     */
    if (pba.band != -1) {
        struct entry *_new = malloc(sizeof(*_new));
        *_new = (struct entry){.lba = lba, .pba = pba, .len = len,
                               .seq = seq, .dirty = 1};
        rb_tree_insert_node(&v->map, _new);
        rb_tree_insert_node(&v->rmap, _new);
    }
}

/*------------------- Initialization ---------------------*/

void read_map_record(struct volume *v, struct map_record *m, uint32_t seq)
{
    assert(m->pba.band < v->n_bands &&
           (m->pba.offset+m->len) <= v->band_size &&
           (m->lba+m->len) <= v->group_span*v->n_groups);
    update_range(v, m->lba, m->len, m->pba, seq);
}

int group_of(struct volume *v, int band)
{
    return (band - 1 - v->map_size) / v->group_size;
}

void read_band_record(struct volume *v, struct band_record *b, uint32_t seq)
{
    assert(b->band < v->n_bands && b->type < BAND_TYPE_MAX &&
           b->pointer <= v->band_size);
    int i = b->band;
    assert(i < v->n_bands);
    v->band[i] = (struct band){.type = b->type, .dirty = 0,
                               .write_pointer = b->pointer, .seq = seq};
    if (b->type == BAND_TYPE_FRONTIER)
        v->groups[group_of(v, i)].frontier = i;
}

struct pba read_records(struct volume *v, struct pba location)
{
    struct header *h = v->buf;
    read_sectors(v, location, v->buf, 1);
    assert(h->magic == STL_MAGIC);

    int i, j;
    uint32_t seq = h->seq;
    struct header h0 = *h;      /* saved copy */

    v->seq = h->seq;
    if (h0.type == RECORD_BAND) {
        struct band_record *r = (void*)h->data;
        for (i = 0; i < h->local_records; i++)
            read_band_record(v, &r[i], seq);
        for (i = j = 0; i < h0.next.offset - location.offset; i++) {
            read_sectors(v, pba_add(v, location, i+1), v->buf, 1);
            r = v->buf;
            struct band_record *end = v->buf + SECTOR_SIZE;
            for (; j < h0.records && (r+i) <= end; i++, j++)
                read_band_record(v, &r[i], seq);
        }
    }
    if (h0.type == RECORD_MAP) {
        struct map_record *m = (void*)h->data;
        for (i = 0; i < h->local_records; i++)
            read_map_record(v, &m[i], h0.seq);
        for (i = j = 0; i < h0.next.offset - location.offset; i++) {
            read_sectors(v, pba_add(v, location, i+1), v->buf, 1);
            m = v->buf;
            struct map_record *end = v->buf + SECTOR_SIZE;
            for (; j < h0.records && (m+i+1) <= end; i++, j++)
                read_map_record(v, &m[i], h0.seq);
        }
    }

    v->map_prev = location;
    return h0.next;
}

/* return the index of the min element
 */
int find_min(int *array, int len)
{
    int i, j, min;
    assert(len > 0);
    for (i = j = 0, min = array[0]; i < len; i++)
        if (array[i] < min) {
            min = array[i];
            j = i;
        }
    return j;
}

struct data_pkt {
    struct header h;
    struct map_record map;
};

/* track them down in order of sequence number and apply them.
 */
int chase_frontiers(struct volume *v)
{
    struct header *h = v->buf;
    struct data_pkt *hdrs = malloc(v->n_groups*sizeof(*hdrs));
    int i, n_valid, dirty = 0, *seq = malloc(v->n_groups*sizeof(int));

    for (i = n_valid = 0; i < v->n_groups; i++) {
        int f = v->groups[i].frontier;
        struct pba f_addr = pba(f, max(0, v->band[i].write_pointer));
        read_sectors(v, f_addr, v->buf, 1);
        if (h->magic == STL_MAGIC) {
            memcpy(&hdrs[n_valid], h, sizeof(hdrs[0]));
            seq[n_valid++] = h->seq;
            //v->band[f].write_pointer = h->next.offset;
        }
    }

    while (n_valid > 0) {
        i = find_min(seq, n_valid);
        assert(seq[i] == hdrs[i].h.seq);
        if (hdrs[i].h.seq > v->seq) {
            dirty = 1;
            if (hdrs[i].h.local_records > 0) 
                read_map_record(v, &hdrs[i].map, seq[i]);
            v->seq = seq[i];
            struct pba next = hdrs[i].h.next;
            v->band[next.band].write_pointer = next.offset;
        }

        /* try to grab the next sector; if not there, then pull out of
         * list. note that this will need fixing for real SMR host-aware.
         */
        read_sectors(v, hdrs[i].h.next, v->buf, 1);
        int this_band = hdrs[i].h.next.band;
        if (h->magic != STL_MAGIC || h->seq < seq[i]) {
            /*
             * stupid C data structures...
             */
            int n = n_valid - i - 1;
            memmove(&hdrs[i], &hdrs[i+1], n * sizeof(hdrs[0]));
            memmove(&seq[i], &seq[i+1], n * sizeof(seq[0]));
            n_valid--;
        }
        else {
            memcpy(&hdrs[i], h, sizeof(hdrs[n_valid]));
            seq[i] = h->seq;
            printf("updating wp[%d] -> %d\n", this_band, h->next.offset);
            //v->band[this_band].write_pointer = h->next.offset;
        }
    }
    free(hdrs);
    free(seq);
    return dirty;
}


struct volume *init_volume(char *dev)
{
    int i, j, k, seq, m;
    struct volume *v = calloc(sizeof(*v), 1);
    v->buf = valloc(SECTOR_SIZE);

    rb_tree_init(&v->map, &fwd_ops);
    rev_ops.rbto_context = v;
    rb_tree_init(&v->rmap, &rev_ops);

    if ((v->fd = open(dev, O_RDWR)) < 0)
        return NULL;

    read(v->fd, v->buf, SECTOR_SIZE);
    struct superblock *sb = v->buf;
    assert(sb->magic == STL_MAGIC);

    v->n_bands = sb->n_bands;
    v->band = calloc(v->n_bands * sizeof(v->band[0]), 1);

    v->map_size = sb->map_size;
    /* need to find current map band and offset */

    v->band_size = sb->band_size;
    v->group_size = sb->group_size;
    v->group_span = sb->group_span;
    v->n_groups = sb->n_groups;
    v->groups = calloc(v->n_groups * sizeof(*v->groups), 1);

    /* Find the current map band - i.e. the one starting with the
     * highest sequence number.
     */
    struct header *h = v->buf;
    for (i = 1, seq = -1; i <= v->map_size; i++) {
        read_sectors(v, pba(i, 0), v->buf, 1);
        if (h->magic == STL_MAGIC && (int)h->seq > seq) {
            seq = h->seq;
            m = i;
        }
    }
    assert(seq > -1);

    /* Find the last record in the current map band - offset of last
     * legal header will be in 'j'
     */
    v->map_band = m;
    for (i = 0, j = -1;;) {
        read_sectors(v, pba(m, i), v->buf, 1);
        if (h->magic != STL_MAGIC || h->seq < seq || h->next.band != m)
            break;
        j = i;
        assert(h->next.offset > i);
        i = h->next.offset;
        seq = h->seq;
    }
    assert(j != -1);
    v->map_offset = i;

    /* now read forward from earliest relevant record to the end of
     * the checkpointed map and band information. Set the volume base
     * accordingly.
     */
    read_sectors(v, pba(m, j), v->buf, 1);
    struct pba base = v->base = h->base;
    seq = h->seq;
    while (base.band != m || base.offset != j)
        base = read_records(v, base);

    /* now we have the band map and the exception map as of the last
     * checkpoint. For now assume that drive is properly checkpointed.
     * In the future, log recovery proceeds in breadth-first / seq#
     * order starting with the tail of all the bands marked 'current'.
     */

    /* recover the state of all of the band groups.
     */
    for (i = 0, j = 1+v->map_size; i < v->n_groups; i++) {
        for (k = 0; k < v->group_size; j++, k++) {
            int type = v->band[j].type;
            if (type == BAND_TYPE_FRONTIER)
                v->groups[i].frontier = j;
            v->groups[i].count[type]++;
        }
    }

    if (chase_frontiers(v))
        checkpoint_volume(v);
    return v;
}

void delete_volume(struct volume *v)
{
    /* first iterate through all the map entries and free them
     */
    struct entry *e = rb_tree_iterate(&v->map, NULL, RB_DIR_RIGHT);
    while (e != NULL) {
        struct entry *tmp = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
        rb_tree_remove_node(&v->map, e);
        rb_tree_remove_node(&v->rmap, e);
        free(e);
        e = tmp;
    }

    /* and then close and free everything else.
     */
    close(v->fd);
    free(v->buf);
    free(v->band);
    free(v->groups);
}

/* ---------- Cleaning ----------- */

/* clean a single group. Returns true if cleaning was performed.
 */
int clean_group(struct volume *v, int g, int minfree, int prio)
{
    int i, nfree, made_changes = 0, iters = 0;
    int base = g * v->group_size + v->map_size + 1;

    /* are there enough free bands?
     */
    for (i = nfree = 0; i < v->group_size; i++)
        if (v->band[base+i].type == BAND_TYPE_FREE)
            nfree++;

    while (v->groups[g].count[BAND_TYPE_FREE] <= minfree) {
        printf("CLEANING %d: free = %d iter %d\n", g,
               v->groups[g].count[BAND_TYPE_FREE], ++iters);
        made_changes = 1;

        assert(iters < 10);

        /* find the cleaning cost of each band, by counting:
         * - total data (sectors) in band
         * - approximate seeks to clean band (cap at ~#tracks/band)
         */
#define SECTORS_PER_SEEK 128
        int *seeks = calloc(v->group_size * sizeof(int), 1);
        int *sectors = calloc(v->group_size * sizeof(int), 1);
        //int seeks[v->group_size], sectors[v->group_size];
        //for (i = 0; i < v->group_size; i++)
        //seeks[i] = sectors[i] = 0;

        struct pba begin = pba(base, 0);
        struct entry *e = rb_tree_find_node_geq(&v->rmap, &begin);

        while (e != NULL && e->pba.band < base+v->group_size) {
            i = e->pba.band - base;
            assert(i >= 0 && i < v->group_size);
            seeks[i]++;
            sectors[i] += e->len;
            e = rb_tree_iterate(&v->rmap, e, RB_DIR_RIGHT);
        }

        /* mark all the free bands so we don't try to clean them
         */
        for (i = 0; i < v->group_size; i++)
            if (v->band[i+base].type == BAND_TYPE_FREE ||
                v->band[i+base].type == BAND_TYPE_FRONTIER)
                sectors[i] = 1e9;

        int min_cost = 1e9, min_band = -1;
        for (i = 0; i < v->group_size; i++) {
#if 0
            int s = min(seeks[i], 1 + v->band_size / 512);
            if (iters > 1)
                s = 0;
            int cost = s*SECTORS_PER_SEEK + sectors[i];
#endif
            int cost = sectors[i];
            if (cost < min_cost) {
                min_cost = cost;
                min_band = i;
            }
        }
        assert(min_band >= 0);

        /* read all the valid extents from this band
         */
        int band = min_band + base, n_sectors = sectors[min_band];
        int n_extents = seeks[min_band];
        void *buf = valloc(n_sectors * SECTOR_SIZE);
        int *len = calloc(n_extents * sizeof(int), 1);
        int64_t *lba = calloc(n_extents * sizeof(int64_t), 1);
        //int   len[n_extents];
        //int64_t lba[n_extents];
        void *ptr = buf;

        printf("PICKED %d - %d sectors\n", band, n_sectors);

        begin = pba(band, 0);
        e = rb_tree_find_node_geq(&v->rmap, &begin);
        for (i = 0; e != NULL && e->pba.band == band; i++) {
            read_sectors(v, e->pba, ptr, e->len);
            len[i] = e->len;
            lba[i] = e->lba;
            ptr += (e->len * SECTOR_SIZE);
            struct entry *tmp = rb_tree_iterate(&v->rmap, e, RB_DIR_RIGHT);
            rb_tree_remove_node(&v->map, e);
            rb_tree_remove_node(&v->rmap, e);
            free(e);
            e = tmp;
        }

        /* Now re-write them
         */
        for (i = 0, ptr = buf; i < n_extents; i++) {
            do_write(v, g, lba[i], ptr, len[i], prio);
            ptr += len[i] * SECTOR_SIZE;
        }

        int type = v->band[band].type;
        v->groups[g].count[type]--;
        v->groups[g].count[BAND_TYPE_FREE]++;
        v->band[band].type = BAND_TYPE_FREE;
        v->band[band].dirty = 1;
        v->band[band].write_pointer = 0;

        free(buf);
        free(seeks); free(sectors);
        free(len); free(lba);
        nfree++;
    }
    return made_changes;
}

/* There are two levels of cleaning and band allocation.
 * - background cleaning - clean until MINFREE_BG bands are free,
 *   allocate extents at normal priority. (clean if 1 free)
 * - forced cleaning - clean until MINFREE_PRIO bands are free,
 *   allocate at high priority. (i.e. can allocate the last band that
 *   is not touched by normal priority allocation)
 */
#define MINFREE_FG 2
#define MINFREE_BG 4
#define PRIO_NORM 0
#define PRIO_HIGH 1

void clean_all(struct volume *v)
{
    int g, dirty;
    for (g = dirty = 0; g < v->n_groups; g++)
        dirty = dirty || clean_group(v, g, MINFREE_BG, PRIO_NORM);
    if (dirty)
        checkpoint_volume(v);
}

/*------------ Write, allocate -----------*/

/* Find a free band in group 'g'.
 * Only grab the last band if 'prio' is true.
 */
int find_free_band(struct volume *v, int g, int prio)
{
    int threshold = prio ? 0 : MINFREE_FG;
    int i, j = 1 + v->map_size + g*v->group_size, n;
    for (i = n = 0; i < v->group_size; i++, j++) {
        if (v->band[j].type == BAND_TYPE_FREE) {
            if (n >= threshold)
                return j;
            n++;
        }
    }
    return -1;
}

/* assemble the header for a write packet into v->buf and write it at
 * the head of 'band'. Updates sequence#.
 * use the same header (with modifications to 'prev' and 'next') at tail.
 */
void do_write_hdr(struct volume *v, struct pba here, struct pba prev,
                  struct pba next, int band, void *map, int n_records)
{
    v->band[band].seq = v->seq;
    memset(v->buf, 0, SECTOR_SIZE);
    struct header *h = v->buf;
    printf("do_write_hdr: %d.%d (%d.%d)\n", here.band, here.offset,
           next.band, next.offset);
    *h = (struct header){.magic = STL_MAGIC, .seq = v->seq++,
                         .type = RECORD_DATA, .local_records = n_records,
                         .records = 0, .prev = prev, .next = next,
                         .base = v->base};
    memcpy(h->data, map, sizeof(struct map_record)*n_records);
    write_sectors(v, here, h, 1);
}

/* allocate a PBA extent. Updates the band map by advancing the write
 * pointer by 'len' sectors, marking it dirty, and setting sequence
 * number. Priority is passed down to find_free_band to avoid deadlock
 * on forced cleaning.
 */
struct pba alloc_extent(struct volume *v, int g, int len, int prio, int *plen)
{
    struct group *gr = v->groups + g;
    int left, b;
top:
    b = gr->frontier;
    left = v->band_size - v->band[b].write_pointer;
    struct pba here = {.band = b, .offset = v->band[b].write_pointer};
    if (left < 8) {
        struct pba prev = {.band = b, .offset = v->band[b].write_pointer-1};
        int b2 = find_free_band(v, g, prio);
        if (b2 == -1) {
            if (clean_group(v, g, MINFREE_FG, PRIO_HIGH))
                checkpoint_volume(v);
            goto top;
        }
        assert(b2 != -1);
        gr->count[BAND_TYPE_FREE]--;
        assert(v->band[b2].write_pointer == 0);
        struct pba next = {.band = b2, .offset = 0};

        gr->count[BAND_TYPE_FULL]++;
        v->band[b].type = BAND_TYPE_FULL;
        v->band[b].dirty = 1;
        v->band[b].write_pointer++;
        v->band[b].seq = v->seq;

        do_write_hdr(v, here, prev, next, 0, 0, 0); /* increments v->seq */

        v->band[b2].type = BAND_TYPE_FRONTIER;
        v->band[b2].dirty = 1;
        v->band[b2].seq = v->seq;

        gr->frontier = b2;
        b = b2;
        here = next;
        left = v->band_size;
    }

    /* caller is responsible for updating write_pointer
     */
    len = min(left-2, len);
    *plen = len;
    printf("alloc: returning %d.%d %d\n", b, v->band[b].write_pointer, len);
    return here;
}

/* actually perform a write, wrapped with DATA records. 'prio' is used
 * to ensure that writes for forced cleaning can grab the last free
 * band.
 *
 * TODO - have alloc_extent return a length, and iterate. That way we
 * don't wast space at the end of a band.
 */
static void do_write(struct volume *v, int group, int64_t lba,
                     void *buf, int sectors, int prio)
{
    int alloced = 0;
    while (sectors > 0) {
        struct pba pba = alloc_extent(v, group, sectors+2, prio, &alloced);
        int _sectors = alloced-2;
        struct map_record map = {.lba = lba, .pba = pba_add(v,pba,1),
                                 .len = _sectors};

        update_range(v, lba, _sectors, pba_add(v, pba, 1), v->seq);

        do_write_hdr(v,
                     pba,                        /* location */
                     pba_add(v, pba, -1),        /* prev */
                     pba_add(v, pba, _sectors+1), /* next */
                     pba.band,
                     &map, 1);      /* one map entry */
        write_sectors(v, pba_add(v, pba, 1), buf, _sectors);
        do_write_hdr(v,
                     pba_add(v, pba, 1+_sectors), /* location */
                     pba,                        /* prev */
                     pba_add(v, pba, _sectors+2), /* next */
                     pba.band,
                     0, 0);         /* no map entry */

        v->band[pba.band].write_pointer += alloced;
        v->band[pba.band].dirty = 1;
        v->band[pba.band].seq = v->seq;

        sectors -= _sectors;
        lba += _sectors;
        buf += _sectors * SECTOR_SIZE;
    }
}

void host_write(struct volume *v, int64_t lba, void *buf, int bytes)
{
    assert(bytes % SECTOR_SIZE == 0);
    int sectors = bytes / SECTOR_SIZE;
    assert(lba + sectors <= v->n_groups * v->group_span);

    /* internal writes can't span a group boundary
     */
    while (sectors > 0) {
        int group = lba / v->group_span;
        int _sectors = min(sectors, (group+1) * v->group_span - lba);
        do_write(v, group, lba, buf, _sectors, PRIO_NORM);
        lba += _sectors;
        sectors -= _sectors;
        buf += (_sectors*SECTOR_SIZE);
    }

    if (v->seq - v->oldest_seq > 3000)
        checkpoint_volume(v);
}

void host_trim(struct volume *v, int64_t lba, int sectors)
{
    assert(lba + sectors <= v->n_groups * v->group_span);

    /* internal ops can't span a group boundary
     */
    while (sectors > 0) {
        struct pba null_pba = {.band = -1, .offset = -1};
        int group = lba / v->group_span;
        int _sectors = min(sectors, (group+1) * v->group_span - lba);
        update_range(v, lba, _sectors, null_pba, v->seq++);
        lba += _sectors;
        sectors -= _sectors;
    }
}

/*----------- Read logic --------------*/

void host_read(struct volume *v, int64_t lba, void *buf, int bytes)
{
    assert(bytes % SECTOR_SIZE == 0);
    int sectors = bytes / SECTOR_SIZE;
    assert(lba + sectors <= v->n_groups * v->group_span);

    for (sectors = bytes / SECTOR_SIZE; sectors > 0; ) {
        struct entry *e = rb_tree_find_node_geq(&v->map, &lba);
        if (e == NULL || e->lba >= lba + sectors) {
            memset(buf, 17, bytes);
            return;
        }
        int64_t pba = pba2long(v, e->pba);
        int offset = max(lba - e->lba, -sectors), len = -offset;
        if (offset < 0) {
            memset(buf, 0, len*SECTOR_SIZE);
            sectors -= len;
            lba += len;
            buf += len * SECTOR_SIZE;
            offset = 0;
        }
        len = min(sectors, e->len - offset);
        if (len > 0)
            read_sectors(v, long2pba(v, pba+offset), buf, len);
        sectors -= len;
        lba += len;
    }
}

/*----------- Map checkpointing --------------*/

/* note - a useful invariant is that accesses to map bands *never* use
 * the write pointers.
 */

void write_meta(struct volume *v, int type, int n_records, struct pba next)
{
    struct header *h = v->buf;
    memset(v->buf, 0, SECTOR_SIZE);
    struct pba location = pba(v->map_band, v->map_offset++);
    if (PBA_EQUAL(next, PBA_NEXT))
        next = pba(v->map_band, v->map_offset);
    *h = (struct header){.magic = STL_MAGIC, .seq = v->seq++,
                         .type = type, .local_records = 0,
                         .records = n_records, .prev = v->map_prev,
                         .next = next, .base = v->base};
    write_sectors(v, location, h, 1);
    v->map_prev = location;
    v->band[v->map_band].write_pointer = v->map_offset;
}

const int map_per_sector = SECTOR_SIZE / sizeof(struct map_record);
const int band_per_sector = SECTOR_SIZE / sizeof(struct band_record);

/* write map updates and then band updates into map band.
 */
void checkpoint_volume(struct volume *v)
{
    /* calculate previous entry
     */
    struct pba next;

    /* make sure there's enough room in the current map band. Note that we 
     * don't bother to checkpoint the write pointers for map bands
     */
    int i = v->map_band;
    int next_band = (i >= v->map_size) ? 1 : i+1;
    int needed = 2 + 20 + 2 + v->n_bands/band_per_sector + 1;
    if (v->map_offset + needed >= v->band_size) {
        write_meta(v, RECORD_NULL,
                   0,                        /* n_records */
                   pba(next_band, 0));       /* next */
        v->map_band = next_band;
        v->map_offset = v->band[i].write_pointer = 0;
    }

    int min_seq = v->seq;
    
    /* checkpoint map entries. always checkpoint the dirty ones.
     */
    int n_records, n_map = rb_tree_count(&v->map);
    struct pba location = pba(v->map_band, v->map_offset);
    uint32_t cutoff = v->oldest_seq;

    /* also roll map updates forward. Assuming each sequence
     * number causes one map update, if we have N map entries and the
     * oldest map entry is 2N sequence numbers ago, it's too old. Roll
     * forward in units of about 10 sectors for efficiency.
     * 'cutoff' is the oldest sequence number that won't get rolled
     * forward. 
     */
    if (v->seq - v->oldest_seq > 2*n_map) 
        cutoff = v->oldest_seq + 10 * map_per_sector;

    /* write out all the band records that are dirty or older than the
     * cutoff.
     */
    struct band_record *bands = valloc(10 * SECTOR_SIZE);
    struct band *b = v->band;
    memset(bands, 0, 10*SECTOR_SIZE);

    for (i = 1+v->map_size, n_records = 0; i < v->n_bands; i++)
        if (b[i].dirty || b[i].seq < cutoff) {
            bands[n_records++] = (struct band_record)
                {.band = i, .pointer = b[i].write_pointer, .type = b[i].type};
            b[i].dirty = 0;
            b[i].seq = v->seq;
        }
        else if (b[i].seq < min_seq) {
            min_seq = b[i].seq;
            /* min_location = b[i].location */
        }

    assert(n_records < 10 * band_per_sector);
    if (n_records > 0) {
        int n_sectors = (n_records + band_per_sector - 1) / band_per_sector;
        next = pba(v->map_band, v->map_offset+1+n_sectors);

        write_meta(v, RECORD_BAND, n_records, next);
        write_sectors(v, pba(v->map_band, v->map_offset), bands, n_sectors);
        v->map_offset += n_sectors;
        write_meta(v, RECORD_BAND, 0 /*n_records*/, PBA_NEXT);
    }
    free(bands);

    /* gather the oldest entries into a checkpoint, and keep track
     * of the next-oldest entry
     */
    struct map_record *map = valloc(20 * SECTOR_SIZE);
    memset(map, 0, 20*SECTOR_SIZE);

    struct entry *e, *min_e = NULL;
    for (n_records = 0, e = rb_tree_iterate(&v->map, NULL, RB_DIR_RIGHT);
         e != NULL; e = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT))
        if (e->seq < cutoff || e->dirty) {
            map[n_records++] = (struct map_record)
                {.lba = e->lba, .pba = e->pba, .len = e->len};
            e->seq = v->seq;
            assert(location.band != 0);
            e->location = location;
            e->dirty = 0;
        } else if (e->seq < min_seq) {
            min_seq = e->seq;
            min_e = e;
        }

    /* next-oldest becomes the new base. (unless there are older
     * bands, in which case we don't update the base)
     */
    assert(n_records < 20 * map_per_sector);
    if (min_e != NULL) {
        v->oldest_seq = min_seq;
        v->base = min_e->location;
    }

    /* don't write anything if nothing changed.
     */
    if (n_records > 0) {
        int n_sectors = (n_records + map_per_sector - 1) / map_per_sector;
        next =  pba(v->map_band, v->map_offset+n_sectors+1);

        write_meta(v, RECORD_MAP, n_records, next);
        write_sectors(v, pba(v->map_band, v->map_offset), map, n_sectors);
        v->map_offset += n_sectors;
        write_meta(v, RECORD_MAP, 0 /* n_records */, PBA_NEXT);
    }
    free(map);
}

/*------------ the rest --------------*/
void print_metadata(struct volume *v)
{
    int i, j;

    printf("bands:\n");
    for (i = 0; i < v->n_bands; ) {
        int type = v->band[i].type, ptr = v->band[i].write_pointer;
        for (j = i; (j+1 < v->n_bands && v->band[j+1].type == type
                     && v->band[j+1].write_pointer == ptr); j++);
        printf("%d-%d %x %s %d %d\n", i, j, v->band[i].type,
               v->band[i].dirty ? "DIRTY" : "",
               v->band[i].write_pointer, (int)v->band[i].seq);
        i = j+1;
    }

    struct entry *e = rb_tree_iterate(&v->map, NULL, RB_DIR_RIGHT);
    while (e != NULL) {
        printf("%d +%d -> %d.%d at %d.%d (%d%s)\n", (int)e->lba, e->len,
               e->pba.band, e->pba.offset, e->location.band, e->location.offset,
               e->seq, e->dirty ? " DIRTY" : "");
        e = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
    }
}
    
void *cmdline_buf;

void cmd_clean(struct volume *v, int argc, char **argv)
{
    clean_all(v);
}

void cmd_checkpoint(struct volume *v, int argc, char **argv)
{
    checkpoint_volume(v);
}

void cmd_print(struct volume *v, int argc, char **argv)
{
    print_metadata(v);
}

void cmd_rprint(struct volume *v, int argc, char **argv)
{
    struct entry *e = rb_tree_iterate(&v->rmap, NULL, RB_DIR_RIGHT);
    while (e != NULL) {
        printf("%d.%d <- %d +%d (%d%s)\n", e->pba.band, e->pba.offset,
               (int)e->lba, e->len, e->seq, e->dirty ? " DIRTY" : "");
        e = rb_tree_iterate(&v->rmap, e, RB_DIR_RIGHT);
    }
}

void cmd_fprint(struct volume *v, int argc, char **argv)
{
    struct entry *e = rb_tree_iterate(&v->map, NULL, RB_DIR_RIGHT);
    while (e != NULL) {
        printf("%d +%d -> %d.%d (%d%s)\n", (int)e->lba, e->len,
               e->pba.band, e->pba.offset, e->seq, e->dirty ? " DIRTY" : "");
        e = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
    }
}

void cmd_read(struct volume *v, int argc, char **argv)
{
    int lba = atoi(argv[1]), len = atoi(argv[2]);
    host_read(v, lba, cmdline_buf, len*SECTOR_SIZE);
}

void cmd_verify(struct volume *v, int argc, char **argv)
{
    int lba = atoi(argv[1]), len = atoi(argv[2]);
    host_read(v, lba, cmdline_buf, len*SECTOR_SIZE);
    int i, val = atoi(argv[3]);
    unsigned char *ptr = cmdline_buf;
    for (i = 0; i < len; i++) 
        if (ptr[i*SECTOR_SIZE] != val) 
            printf("bad value %d (%d) at %d\n", ptr[i*SECTOR_SIZE], val, lba+i);
}

void cmd_write(struct volume *v, int argc, char **argv)
{
    int lba = atoi(argv[1]), len = atoi(argv[2]), value = 0;
    if (argc > 3)
        value = atoi(argv[3]);
    memset(cmdline_buf, value, len*SECTOR_SIZE);
    host_write(v, lba, cmdline_buf, len*SECTOR_SIZE);
}

void map_overlap(struct volume *v)
{
    struct entry *e = rb_tree_iterate(&v->map, NULL, RB_DIR_RIGHT);
    int bad = 0;
    while (e != NULL) {
        struct entry *tmp = rb_tree_iterate(&v->map, e, RB_DIR_RIGHT);
        if (tmp && e->lba + e->len > tmp->lba) {
            printf("  error: %d+%d overlaps %d+%d\n", (int)e->lba, e->len,
                   (int)tmp->lba, tmp->len);
            bad = 1;
        }
        e = tmp;
    }
    if (!bad)
        printf("OK\n");
}

void rmap_overlap(struct volume *v)
{
    struct entry *e = rb_tree_iterate(&v->rmap, NULL, RB_DIR_RIGHT);
    int bad = 0;
    while (e != NULL) {
        struct entry *tmp = rb_tree_iterate(&v->rmap, e, RB_DIR_RIGHT);
        if (tmp && e->pba.band == tmp->pba.band &&
            e->pba.offset + e->len > tmp->pba.offset) {
            printf("  error: %d.%d+%d overlaps %d.%d+%d\n",
                   e->pba.band, e->pba.offset, e->len,
                   tmp->pba.band, tmp->pba.offset, tmp->len);
            bad = 1;
        }
        if (tmp) {
            int64_t pba1 = pba2long(v, e->pba);
            int64_t pba2 = pba2long(v, tmp->pba);
            if (pba1 + e->len > pba2) {
                printf("  error2: %d.%d+%d overlaps %d.%d+%d\n",
                       e->pba.band, e->pba.offset, e->len,
                       tmp->pba.band, tmp->pba.offset, tmp->len);
                bad = 1;
            }
        }
        e = tmp;
    }
    if (!bad)
        printf("OK\n");
}
    
void cmd_overlap(struct volume *v, int argc, char **argv)
{
    if (!strcmp(argv[1], "fwd"))
        map_overlap(v);
    else if (!strcmp(argv[1], "rev"))
        rmap_overlap(v);
    else
        printf("ERROR: bad command: overlap %s\n", argv[1]);
}

void cmd_break(struct volume *v, int argc, char **argv)
{
}

void cmd_trim(struct volume *v, int argc, char **argv)
{
    int lba = atoi(argv[1]), len = atoi(argv[2]);
    host_trim(v, lba, len);     /* len in sectors */
}

struct {
    char *cmd;
    void (*fn)(struct volume *, int, char **);
} cmdtable[] = {
    {.cmd = "clean", .fn=cmd_clean},
    {.cmd = "checkpoint", .fn=cmd_checkpoint},
    {.cmd = "print", .fn=cmd_print},
    {.cmd = "rprint", .fn=cmd_rprint},
    {.cmd = "fprint", .fn=cmd_fprint},
    {.cmd = "read", .fn=cmd_read},
    {.cmd = "verify", .fn=cmd_verify},
    {.cmd = "write", .fn=cmd_write},
    {.cmd = "break", .fn=cmd_break},
    {.cmd = "trim", .fn=cmd_trim},
    {.cmd = "overlap", .fn=cmd_overlap}
};

void verify_free(struct volume *v)
{
    int g, i, nf;
    for (g = 0; g < v->n_groups; g++) {
        int base = 1 + v->map_size + v->group_size * g;
        for (i = nf = 0; i < v->group_size; i++)
            if (v->band[base+i].type == BAND_TYPE_FREE)
                nf++;
        assert(nf == v->groups[g].count[BAND_TYPE_FREE]);
    }
}

int main(int argc, char **argv)
{
    struct volume *v = init_volume(argv[1]);
    print_metadata(v);

    char line[80];
    cmdline_buf = malloc(0x1000000); /* 16M */
    while (fgets(line, sizeof(line), stdin) != NULL) {
        char **ap, *av[10], *ptr;
        printf("> %s", line);
        if (line[0] == '#' || line[0] == '\n')
            continue;
        for (ap = av, ptr = line; (*ap = strsep(&ptr, " \t\n")) != NULL;)
            if (**ap != '\0')
                if (++ap >= &av[10])
                    break;
        int i, ac = ap-&av[0],
            n_cmds = sizeof(cmdtable)/sizeof(cmdtable[0]);
        if (ac == 0)
            continue;
        if (!strcmp(av[0], "quit"))
            break;
        if (!strcmp(av[0], "close"))
            delete_volume(v);
        else if (!strcmp(av[0], "open"))
            v = init_volume(argv[1]);
        else {
            for (i = 0; i < n_cmds; i++) 
                if (!strcmp(av[0], cmdtable[i].cmd)) {
                    cmdtable[i].fn(v, ac, av);
                    break;
                }
            if (i == n_cmds)
                printf("invalid command: %s\n", av[0]);
        }
    }
    return 0;
}
