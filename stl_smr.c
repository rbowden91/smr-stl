#include <unistd.h>
#include <sys/uio.h>
#include <assert.h>
#include <pthread.h>
#include <libaio.h>

typedef int64_t lba_t;
#define _INTERNAL 1
#include "stl_smr.h"

int smr_n_bands(struct smr *dev)
{
    return dev->n_bands;
}

int smr_band_size(struct smr *dev)
{
    return dev->band_size;
}

int smr_write_pointer(struct smr *dev, int band)
{
    assert(band >= 0 && band < dev->n_bands);
    return dev->write_pointers[band];
}

/* read from a given band and offset. Note that unsigned values make
 * length checking a bit easier...
 */
void smr_read(struct smr *dev, unsigned band, unsigned offset, void *buf,
              unsigned n_sectors)
{
    assert(band < dev->n_bands && offset < dev->band_size &&
           offset+n_sectors <= dev->write_pointers[band]);

    off_t position = (dev->band_size * (off_t)band + offset) * SECTOR_SIZE;
    int val = pread(dev->fd, buf, n_sectors*SECTOR_SIZE, position);
    assert(val > 0);
}

void smr_read_multi(struct smr *dev, struct smr_op *ops, int opcount)
{
    struct iocb _cbs[opcount], *cbs[opcount];
    io_context_t ctx;
    int i, val;
    
    memset(&ctx, 0, sizeof(ctx));
    memset(_cbs, 0, sizeof(_cbs));
    for (i = 0; i < opcount; i++)
        cbs[i] = _cbs + i;
    
    val = io_setup(opcount, &ctx);
    assert(val >= 0);

    for (i = 0; i < opcount; i++) {
        int64_t offset = (dev->band_size*(off_t)ops[i].band + ops[i].offset) *
            SECTOR_SIZE; 
        io_prep_pread(cbs[i], dev->fd, ops[i].buf, ops[i].len * SECTOR_SIZE,
                      offset);
    }
    val = io_submit(ctx, opcount, cbs);
    assert(val >= 0);

    struct io_event events[opcount];
    memset(events, 0, sizeof(events));

    val = io_getevents(ctx, opcount, opcount, events, NULL);
    assert(val >= 0);

    val = io_destroy(ctx);
    assert(val >= 0);
}

#if 0
/* all writes have to obey the shingling constraint
 */
void smr_write_multi(struct smr *dev, struct smr_op *ops, int opcount)
{
    int i;
    struct aiocb *list[opcount];
    struct aiocb _list[opcount];

    memset(_list, 0, sizeof(_list));

    for (i = 0; i < opcount; i++) {
        int b = ops[i].band;
        assert(ops[i].offset == dev->write_pointers[b]);
        dev->write_pointers[b] += ops[i].len;
        assert(dev->write_pointers[b] <= dev->band_size);

        list[i] = &_list[i];
        list[i]->aio_fildes = dev->fd;
        list[i]->aio_buf = ops[i].buf;
        list[i]->aio_nbytes = ops[i].len * SECTOR_SIZE;
        list[i]->aio_offset =
            (dev->band_size*(off_t)ops[i].band + ops[i].offset) * SECTOR_SIZE;
        list[i]->aio_lio_opcode = LIO_WRITE;
    }
    int val = lio_listio(LIO_WAIT, list, i, NULL);
    if (val < 0) 
        for (i = 0; i < opcount; i++)
            printf("%d %d\n", i, (int)aio_return(list[i]));
    assert(val >= 0);
}

void smr_write(struct smr *dev, unsigned band, unsigned offset, const void *buf,
               unsigned n_sectors)
{
    struct smr_op op = {.band = band, .offset = offset, .len = n_sectors,
                        .buf = (void*)buf};
    smr_write_multi(dev, &op, 1);
}
#endif

static int iov_sum(const struct iovec *iov, int iovcnt)
{
    int i, sum;
    for (i = sum = 0; i < iovcnt; i++)
        sum += iov[i].iov_len;
    return sum / SECTOR_SIZE;
}
   
void smr_writev(struct smr *dev, unsigned band, unsigned offset,
                struct iovec *iov, int iovcnt)
{
    int n_sectors = iov_sum(iov, iovcnt);
    assert(band < dev->n_bands && offset+n_sectors <= dev->band_size);
    off_t position = (dev->band_size * (off_t)band + offset) * SECTOR_SIZE;

    /* re-order any threads which are trying to write out-of-order. Use
     * simple thundering-herd logic and do the write under the lock.
     */
    pthread_mutex_lock(&dev->m);
    while (offset != dev->write_pointers[band])
        pthread_cond_wait(&dev->c, &dev->m);

    dev->write_pointers[band] += n_sectors;
    pthread_cond_broadcast(&dev->c); /* thunder the herd */
    pthread_mutex_unlock(&dev->m);
    
    int val = pwritev(dev->fd, iov, iovcnt, position);
    assert(val > 0);
}

void smr_write(struct smr *dev, unsigned band, unsigned offset, const void *buf,
                unsigned n_sectors)
{
    struct iovec v = {.iov_base = (void*)buf, .iov_len = n_sectors*SECTOR_SIZE};
    return smr_writev(dev, band, offset, &v, 1);
}

void smr_write_multi(struct smr *dev, struct smr_op *ops, int opcount)
{
    struct iovec iov[opcount];
    int i;
    for (i = 0; i < opcount; i++) {
	iov[i] = (struct iovec){.iov_base = ops[i].buf,
				.iov_len = ops[i].len * SECTOR_SIZE};
	if (i > 0)
	    assert(ops[i].band == ops[i-1].band &&
		   ops[i].offset == ops[i-1].offset + ops[i-1].len);
    }
    smr_writev(dev, ops[0].band, ops[0].offset, iov, opcount);
}

/* I really should factor this and read_multi...
 */
void smr_write_multi_bak(struct smr *dev, struct smr_op *ops, int opcount)
{
    struct iocb _cbs[opcount], *cbs[opcount];
    io_context_t ctx;
    int i, val;
    
    memset(&ctx, 0, sizeof(ctx));
    memset(_cbs, 0, sizeof(_cbs));
    for (i = 0; i < opcount; i++)
        cbs[i] = _cbs + i;
    
    val = io_setup(opcount, &ctx);
    assert(val >= 0);

    /* all modification of write_pointers is under this lock
     */
    pthread_mutex_lock(&dev->m);

    /* re-order any threads which are trying to write out-of-order. Use
     * simple thundering-herd logic and do the write under the lock.
     */
    int band = ops[0].band, offset = ops[0].offset;
    while (offset != dev->write_pointers[band])
        pthread_cond_wait(&dev->c, &dev->m);

    for (i = 0; i < opcount; i++) {
        int64_t b = ops[i].band;
        assert(ops[i].offset == dev->write_pointers[b]);
        dev->write_pointers[b] += ops[i].len;
        assert(dev->write_pointers[b] <= dev->band_size);

        int64_t position = (dev->band_size * b + ops[i].offset) * SECTOR_SIZE; 
        io_prep_pwrite(cbs[i], dev->fd, ops[i].buf, ops[i].len * SECTOR_SIZE,
                       position);
    }

    val = io_submit(ctx, opcount, cbs);
    assert(val >= 0);
    pthread_mutex_unlock(&dev->m);
    
    struct io_event events[opcount];
    memset(events, 0, sizeof(events));

    val = io_getevents(ctx, opcount, opcount, events, NULL);
    assert(val >= 0);

    val = io_destroy(ctx);
    assert(val >= 0);
}

/* causes underlying driver to issue FLUSH or SYNCHRONIZE CACHE
 */
void smr_flush(struct smr *dev)
{
    fsync(dev->fd);
}
