/*
 * file:        stl_realsmr.c
 * description: real SMR functions 
 */


#define _LARGEFILE64_SOURCE /*for lseek64*/
#define _BSD_SOURCE /* for strtoll() */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#define __USE_GNU   /* for O_DIRECT */
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <endian.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <endian.h>
#include <asm/byteorder.h>
#include <assert.h>
#include <sys/uio.h>
#include <pthread.h>
#include <libaio.h>

#include "sgio.h"

typedef int64_t lba_t;
#define _INTERNAL 1
#include "stl_smr.h"

struct zone_list_header {
    __u32 n_zones;
    __u8  same;
    __u8  pad[3];
    __u64 max_lba;
};

struct zone_list_entry {
    __u8  zone_type;
    __u8  reset : 1;
    __u8  non_seq : 1;
    __u8  _pad : 2;
    __u8  condition : 4;
    __u8  _pad2[6];
    __u64 length;
    __u64 start;
    __u64 write_ptr;
    __u8  _pad3[32];
};

void do_report_zones (int fd, __u64 lba, void *data, unsigned int data_bytes)
{
    struct ata_tf tf;
    unsigned int data_sects = (data_bytes + 511) / 512;     

    tf_init(&tf, 0x4A, 0, data_sects);
    tf.lob.feat = 0x00;

    tf.lob.lbal = lba & 0xFF;
    tf.lob.lbam = (lba >> 8) & 0xFF;
    tf.lob.lbah = (lba >> 16) & 0xFF;
    tf.is_lba48 = 1;
    tf.hob.lbal = (lba >> 24) & 0xFF;
    tf.hob.lbam = (lba >> 32) & 0xFF;
    tf.hob.lbah = (lba >> 40) & 0xFF;

    if (sg16(fd, SG_READ, SG_DMA, &tf, data, data_bytes, 300 /*s*/)) {
        perror("FAILED");
    } 
}

void do_reset_write_pointer (int fd, __u64 lba,
                                   int all_bit)
{
    struct ata_tf tf;

    tf_init(&tf, 0x9F, 0, 0);
        tf.lob.feat = 4;        /* ATA_SUBCMD_RESET_WP */
    tf.hob.feat = all_bit;
    //tf.hob.feat = 0;

  
    tf.is_lba48 = 1;
    tf.lob.lbal = lba & 0xFF;
    tf.lob.lbam = (lba >> 8) & 0xFF;
    tf.lob.lbah = (lba >> 16) & 0xFF;
    tf.hob.lbal = (lba >> 24) & 0xFF;
    tf.hob.lbam = (lba >> 32) & 0xFF;
    tf.hob.lbah = (lba >> 40) & 0xFF;

    if (sg16(fd, SG_READ, SG_DMA, &tf, 0, 0, 300)) {
        perror("FAILED");
    } 
}

void smr_reset_pointer(struct smr *dev, unsigned band) 
{
    __u64 band_lba = band * dev->band_size * 8;
    do_reset_write_pointer(dev->fd, band_lba, 0);
    dev->write_pointers[band] = 0;
}

void smr_reset_all(struct smr *dev) 
{
    do_reset_write_pointer(dev->fd, 0, 1);
    int i;
    for (i = 0; i < dev->n_bands; i++)
        dev->write_pointers[i] = 0;
}

struct smr *smr_open(const char *name)
{
    struct smr *dev = NULL;
    void *write_pointers = NULL;

    dev = calloc(sizeof(struct smr), 1);
    pthread_mutex_init(&dev->m, NULL);
    pthread_cond_init(&dev->c, NULL);
    
    int fd = open(name, O_RDWR|O_DIRECT);
    if (fd < 0)
        return NULL;

    unsigned int i, len = 512*512;
    void *data = malloc(len);
    struct zone_list_header *hdr = data;
    struct zone_list_entry *entry = data;

    do_report_zones(fd, 0, data, len);

    dev->n_bands = hdr->n_zones;
    dev->band_size = (int)entry[1].length / 8;

    if ((hdr->same & 3) == 2 ) {
        dev->n_bands--;
    }

    write_pointers = malloc(dev->n_bands * sizeof(int));
    dev->write_pointers = write_pointers;

    __u64 max_lba = hdr->max_lba, lba = 0;
    int j = 0;        
    while (lba < max_lba && j < dev->n_bands) {
        do_report_zones(fd, lba, data, len);
        for (i = 1;
	     i < len/sizeof(*entry) && lba<max_lba && j < dev->n_bands; i++) {
	    dev->write_pointers[j] = (entry[i].write_ptr-lba) / 8;
	    j++;
	    lba = entry[i].start + entry[i].length;
        }
    }

    dev->fd = fd;
    if (data)
        free(data);

    return dev;
}

void smr_close(struct smr *dev)
{
    close(dev->fd);
    free(dev);
}

