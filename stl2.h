#define STL_MAGIC 0x4c54537f

/* band 0 */
struct superblock {
    uint32_t magic;
    uint64_t disk_size;         /* 4K sectors */
    uint32_t n_bands;
    uint32_t band_size;         /* 4K sectors */
    uint32_t group_size;        /* in bands */
    uint32_t group_span;        /* in 4K sectors */
    uint32_t n_groups;
    uint32_t map_size;          /* in bands */
};

/* map is in bands 1..map_size */
/* group 0 starts at band map_size-1 */

enum band_type {
    BAND_TYPE_FREE,
    BAND_TYPE_FULL,
    BAND_TYPE_FRONTIER,
    BAND_TYPE_MAX
};

//#define BAND_CURRENT 0x80

enum {
    RECORD_BAND=1,
    RECORD_MAP=2,
    RECORD_DATA=3,
    RECORD_NULL=4
};

struct band_record {
    uint32_t band;
    uint32_t pointer;
    uint8_t  type;
} __attribute__((packed));

struct pba {
    int32_t band;
    int32_t offset;
};

struct map_record {
    int64_t    lba;
    struct pba pba;
    int32_t    len;
};

struct header {
    uint32_t   magic;
    uint32_t   seq;
    uint16_t   type;
    uint16_t   local_records;
    uint32_t   records;
    struct pba next;
    struct pba prev;
    struct pba base;            /* only for MAP / BAND? */
    char       data[0];         /* MAP,DATA: map_record */
                                /* BAND: band_record */
};

#define SECTOR_SIZE 4096ULL
