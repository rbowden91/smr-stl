/*
 * file:        stl-plugin.c
 * description: NBD plugin for shingled translation layer
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nbdkit-plugin.h>
#include <assert.h>
#include "stl.h"
#include "stl_public.h"

#define THREAD_MODEL NBDKIT_THREAD_MODEL_PARALLEL
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

void *smr_dev;

int stlplugin_config(const char *key, const char *value)
{
    if (!strcmp(key, "device")) {
        if (!(smr_dev = stl_open(value))) {
            nbdkit_error("failed to open %s\n", value);
            return -1;
        }
        printf("device %s\n", value);
        return 1;
    }
    else if (!strcmp(key, "logfile")) {
        if (!stl_loginit(value)) {
            nbdkit_error("failed to open logfile %s\n", value);
            return -1;
        }
        printf("logfile %s\n", value);
        return 1;
    }
    else {
        nbdkit_error("bad option: %s=%s\n", key, value);
        return -1;
    }
}

int stlplugin_config_complete(void)
{
    if (!smr_dev) {
        nbdkit_error("no device specified\n");
        return -1;
    }
    return 1;
}

void *stlplugin_open(int readonly)
{
    return smr_dev;
}

void stlplugin_close(void *handle)
{
    stl_close(smr_dev);
    exit(0);
}

int64_t stlplugin_get_size(void *handle)
{
    return stl_vsize(smr_dev) * STL_SECTOR_SIZE;
}

int stlplugin_is_rotational(void *handle)
{
    return 1;
}

int stlplugin_can_trim(void *handle)
{
    return 0;
}

int stlplugin_can_flush (void *handle)
{
    return 1;
}

int stlplugin_flush (void *handle)
{
    return 0;
}

int stlplugin_pread(void *handle, void *buf, uint32_t count, uint64_t offset)
{
    int i = offset % STL_SECTOR_SIZE;

    if (i != 0) {
        nbdkit_debug("unaligned READ\n");
        void *tmp = valloc(STL_SECTOR_SIZE);
        stl_read(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        int len = min(STL_SECTOR_SIZE - i, count);
        memcpy(buf, tmp+i, len);
        buf += len;
        offset += len;
        count -= len;
        free(tmp);
    }

    int n_sectors = count / STL_SECTOR_SIZE;
    if (n_sectors > 0)
        stl_read(smr_dev, offset/STL_SECTOR_SIZE, buf, n_sectors);
    
    buf += n_sectors * STL_SECTOR_SIZE;
    offset += n_sectors * STL_SECTOR_SIZE;
    count -= n_sectors * STL_SECTOR_SIZE;
    
    if (count > 0) {
        nbdkit_debug("unaligned READ length\n");
        void *tmp = valloc(STL_SECTOR_SIZE);
        stl_read(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        memcpy(buf, tmp, count);
        free(tmp);
    }

    return 0;
}

int stlplugin_pwrite(void *handle, const void *buf, uint32_t count,
                     uint64_t offset)
{
    int i = offset % STL_SECTOR_SIZE;
    
    if (i != 0) {
        nbdkit_debug("unaligned WRITE\n");
        void *tmp = valloc(STL_SECTOR_SIZE);
        stl_read(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        int len = min(STL_SECTOR_SIZE - i, count);
        memcpy(tmp+i, buf, len);
        stl_write(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        buf += len;
        offset += len;
        count -= len;
        free(tmp);
    }

    int n_sectors = count / STL_SECTOR_SIZE;
    if (n_sectors > 0)
        stl_write(smr_dev, offset/STL_SECTOR_SIZE, buf, n_sectors);

    buf += n_sectors * STL_SECTOR_SIZE;
    offset += n_sectors * STL_SECTOR_SIZE;
    count -= n_sectors * STL_SECTOR_SIZE;
    
    if (count > 0) {
        nbdkit_debug("unaligned WRITE length\n");
        void *tmp = valloc(STL_SECTOR_SIZE);
        stl_read(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        memcpy(tmp, buf, count);
        stl_write(smr_dev, offset/STL_SECTOR_SIZE, tmp, 1);
        free(tmp);
    }

    return 0;
}


int stlplugin_trim(void *handle, uint32_t count, uint64_t offset)
{
    int n = offset % STL_SECTOR_SIZE;
    offset += n;
    count -= n;
    if (n != 0 || count % STL_SECTOR_SIZE != 0)
        nbdkit_debug("unaligned TRIM\n");

    stl_trim(smr_dev, offset / STL_SECTOR_SIZE, count / STL_SECTOR_SIZE);

    return 0;
}

static struct nbdkit_plugin plugin = {
  .name              = "STLplugin",
  .config            = stlplugin_config,
  .config_complete   = stlplugin_config_complete,
  .open              = stlplugin_open,
  .close             = stlplugin_close,
  .is_rotational     = stlplugin_is_rotational,
  .can_trim          = stlplugin_can_trim,
  .get_size          = stlplugin_get_size,
  .pread             = stlplugin_pread,
  .pwrite            = stlplugin_pwrite,
  .trim              = stlplugin_trim,
  .can_flush         = stlplugin_can_flush,
  .flush             = stlplugin_flush
};

NBDKIT_REGISTER_PLUGIN(plugin)
